<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="fontiran.com:license" content="Y68A9">
    <link rel="icon" href="../build/images/favicon.ico" type="image/ico"/>
 

   <!-- Bootstrap -->
   <link href="../views/css/bootstrap.min.css" rel="stylesheet">
    <link href="../views/css/bootstrap-rtl.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../views/css/fontawesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../views/css/nprogress.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="../views/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../views/css/green.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../views/css/daterangepicker.css" rel="stylesheet">
    

    <!-- Custom Theme Style -->
    <link href="../views/css/custom.min.css" rel="stylesheet">
    <style>
       .tbl1{
           margin:0 auto;
       }
       .tbl1 td{
           padding:0 5px;
           font-weight:bold;
       }
    </style>
</head>
<!-- /header content -->
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col hidden-print">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
                </div>

                <div class="clearfix"></div>

              <!-- menu profile quick info -->
              <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="../views/img/img.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>مرحبا بك</span>
                        <h2><?= 'Admin '; ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->
                <br/>

                <!-- sidebar menu -->
              
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>لوحة تحكم</h3>
                       
                        <ul class="nav side-menu">
                            <li><a href="../adminger/"><i class="fa fa-home"></i>الرئيسية<span class="fa fa-chevron-down"></span></a>
                            </li>
                            <li><a><i class="fa fa-edit"></i> حسابات المستخدمين <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/accounts">إدارة الحسابات</a></li>
                                    <li><a href="../adminger/chang_pwd">تغير كلمة المرور</a></li>
                                    <li><a href="../adminger/chang_pwd">صلاحيات</a></li>
                                   <!--  <li><a href="../adminger/chang_pwd">إضافة مناطق</a></li>
                                    <li><a href="../adminger/chang_pwd">إضافة مدن</a></li>
                                    <li><a href="../adminger/chang_pwd">أنواع الحسابات</a></li> -->
                                    <li><a href="../adminger/chang_pwd">تقارير</a></li>
                                </ul>
                            </li>

                            <li><a><i class="fa fa-desktop"></i> السيارات <span
                                    class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/cars">إضافة سيارة جديدة</a></li>
                                    <li><a href="../adminger/barnd">إضافة علامة تجارية جديدة - براند</a></li>
                                    <li><a href="../adminger/model"> موديل </a></li>
                                    <li><a href="../adminger/type"> أنواع السيارات </a></li>  
                                    <li><a href="../adminger/manifctor"> مصانع السيارات </a></li>                                 
                                    <!--<li><a href="../adminger/parts">قطع غيار</a></li>
                                    <li><a href="typography.html">خدمات</a></li>
                                    <li><a href="icons.html">إحصائيات</a></li>
                                    <li><a href="glyphicons.html">تقارير</a></li>  -->
                                </ul>
                            </li>
                            
                            <li><a><i class="fa fa-table"></i> قطع الغيار <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/parts">إضافة قطع غيار</a></li>
                                    <li><a href="../adminger/parts_type">أنواع القطع</a></li>
                                </ul>
                            </li>

                            <li><a><i class="fa fa-table"></i>  الخدمات <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                <li><a href="../adminger/services_type"> أنواع الخدمات المتاحة</a></li>
                                <li><a href="../adminger/services"> الخدمات</a></li>
                                <li><a href="../adminger/technicians_Specfic"> تخصصات الفنيين  </a></li>
                                <li><a href="../adminger/technicians"> الفنيين  </a></li>
                                 <!-- <li><a href="../adminger/maintenance_workshops"> ورش صيانة</a></li>
                                   <li><a href="../adminger/accessories">مستلزمات السيارة </a></li>
                                    <li><a href="../adminger/compy_padding"> شركات التنجيد </a></li>
                                    <li><a href="../adminger/technicians"> الفنيين  </a></li> -->
                                </ul>
                            </li>
                            

                            <li><a><i class="fa fa-table"></i> الإعدادات <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                
                                <li><a href="../adminger/store"> المتجر </a></li>
                            <li><a href="../adminger/Purchases">  إضافة كمية </a></li> 
                            <li><a href="../adminger/haraj_type">  أنواع الحراج </a></li>
                            <li><a href="../adminger/haraj">  قائمة الحراج </a></li>
                            <li><a href="../adminger/delivery"> التوصيل </a></li>
                            <li><a href="../adminger/area">المنطقة</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>
                 <!-- /menu footer buttons -->
                 <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="تنظیمات">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="تمام صفحه" onclick="toggleFullScreen();">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="قفل" class="lock_btn">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="خروج" href="../adminger/signin">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav hidden-print">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="../views/img/img.jpg" alt=""> Admin
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:;"> نمایه</a></li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>تنظیمات</span>
                                    </a>
                                </li>
                                <li><a href="javascript:;">کمک</a></li>
                                <li><a href="../adminger/signin"><i class="fa fa-sign-out pull-right"></i> خروج</a></li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="../build/images/img.jpg"
                                                                 alt="Profile Image"/></span>
                                        <span>
                          <span>Admin </span>
                          <span class="time">3 دقیقه پیش</span>
                        </span>
                                        <span class="message">
                          فیلمای فستیوال فیلمایی که اجرا شده یا راجع به لحظات مرده ایه که فیلمسازا میسازن. آنها جایی بودند که....
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="../build/images/img.jpg"
                                                                 alt="Profile Image"/></span>
                                        <span>
                          <span> Admin</span>
                          <span class="time">3 دقیقه پیش</span>
                        </span>
                                        <span class="message">
                          فیلمای فستیوال فیلمایی که اجرا شده یا راجع به لحظات مرده ایه که فیلمسازا میسازن. آنها جایی بودند که....
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="../build/images/img.jpg"
                                                                 alt="Profile Image"/></span>
                                        <span>
                          <span> Admin</span>
                          <span class="time">3 دقیقه پیش</span>
                        </span>
                                        <span class="message">
                          فیلمای فستیوال فیلمایی که اجرا شده یا راجع به لحظات مرده ایه که فیلمسازا میسازن. آنها جایی بودند که....
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="../build/images/img.jpg"
                                                                 alt="Profile Image"/></span>
                                        <span>
                          <span> Admin</span>
                          <span class="time">3 دقیقه پیش</span>
                        </span>
                                        <span class="message">
                          فیلمای فستیوال فیلمایی که اجرا شده یا راجع به لحظات مرده ایه که فیلمسازا میسازن. آنها جایی بودند که....
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a>
                                            <strong>مشاهده تمام اعلان ها</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
        <!-- /header content -->
        
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>أنواع السيارات</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" >
                        <span class="input-group-btn">
                              <button class="btn btn-default" type="button">ابحث!</button>
                          </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>  
                            <small></small>
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">تنظیمات 1</a>
                                    </li>
                                    <li><a href="#">تنظیمات 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <form class="form-horizontal form-label-left" action="../product_type/new" method="POST">

                           <!-- <p>برای کتابخانه جایگزین برای اعتبار سنجی در صفحه فرم عمومی <code>parsleyJS</code> را نگاه کنید <a
                                    href="form.html">فرم عمومی</a>
                            </p> -->
                            <span class="section">البيانات</span>

                           
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="b_title"> الاسم <span
                                        class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input  type="text" name="pt_type" required="required"  class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>


                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea"> الوصف<span
                                        class="required">
                                        
                                        </span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea required="required" name="pt_desc" class="form-control col-md-7 col-xs-12"></textarea>
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea"> ملاحظات - تعليق<span
                                        class="required">
                                        
                                        </span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea required="required" name="pt_comment" class="form-control col-md-7 col-xs-12"></textarea>
                                </div>
                            </div>
                                 <center>
                                    <button id="send" type="submit" class="btn btn-success" style="width:40%;">حفظ</button>
                                </center>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<!-- /page content -->

        <!-- footer content -->
        <footer class="hidden-print">
            <div class="pull-left">
            <!-- Gentelella - قالب پنل مدیریت بوت استرپ <a href="https://colorlib.com">Colorlib</a> | پارسی شده توسط <a
                    href="https://morteza-karimi.ir">مرتضی کریمی</a>-->
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
<div id="lock_screen">
    <table>
        <tr>
            <td>
                <div class="clock"></div>
                <span class="unlock">
                    <span class="fa-stack fa-5x">
                      <i class="fa fa-square-o fa-stack-2x fa-inverse"></i>
                      <i id="icon_lock" class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                </span>
            </td>
        </tr>
    </table>
</div>
<!-- jQuery -->
<script src="../views/js/jquery-3.5.1.min.js"></script>
<!-- Bootstrap -->
<script src="../views/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../views/js/fastclick.js"></script>
<!-- NProgress -->
<script src="../views/js/nprogress.js"></script>
<!-- bootstrap-progressbar -->
<script src="../views/js/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../views/js/icheck.min.js"></script>

<!-- bootstrap-daterangepicker -->
<script src="../views/js/moment.min.js"></script>

<script src="../views/js/daterangepicker.js"></script>

<!-- validator -->
<script src="../views/js/validator.js"></script>

<!-- Custom Theme Scripts -->
<script src="../views/js/custom.min.js"></script>



</body>
</html>
