<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="fontiran.com:license" content="Y68A9">
    <link rel="icon" href="../build/images/favicon.ico" type="image/ico"/>
 

   <!-- Bootstrap -->
   <link href="../views/css/bootstrap.min.css" rel="stylesheet">
    <link href="../views/css/bootstrap-rtl.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../views/css/fontawesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../views/css/nprogress.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="../views/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../views/css/green.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../views/css/daterangepicker.css" rel="stylesheet">
    <link href='../views/css/jquery.alertable.css' rel="stylesheet">
    <link href="../views/css/animate.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../views/css/custom.min.css" rel="stylesheet">
    <style>

       #update,#reset{
           display: none;
       }

       .tbl1{
           /*margin:0 auto;*/
       }
       .tbl1 select{
        width: 220px;
        border: 1px solid #aaa;
       }
       .tbl1 td{
           padding:3px 0px;
           font-weight:bold;
       }
       .form-horizontal .form-group{
           width: 500px;
       }
       .x_content .table td{
           font-size: 12px;
           vertical-align: bottom;
       }
    </style>
</head>
<!-- /header content -->
<body class="nav-md">
<div class="container body">
    <div class="main_container">
    <div class="col-md-3 left_col hidden-print">
            <div class="left_col scroll-view">
                <!--<div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
                </div>-->

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                    <img src="../views/img/img.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                    <span>مرحبا بك</span>
                        <h2><?= 'Admin '; ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br/>

                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>لوحة تحكم</h3>
                        <ul class="nav side-menu">
                            <li><a href="../adminger/"><img src="../views/img/home.png" width="18" height="18"/> الرئيسية </a>
                            </li>
                            <li><a><img src="../views/img/user.png" width="18" height="18"/> حسابات الأعضاء <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/accounts">إدارة الحسابات</a></li>
                                    <li><a href="../adminger/chang_pwd">تغير كلمة المرور</a></li>
                                  <!--  <li><a href="../adminger/chang_pwd">صلاحيات</a></li>
                                    <li><a href="../adminger/chang_pwd">إضافة مناطق</a></li>
                                    <li><a href="../adminger/chang_pwd">إضافة مدن</a></li>
                                    <li><a href="../adminger/chang_pwd">أنواع الحسابات</a></li> -->
                                    <li><a href="../adminger/chang_pwd">تقارير</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/mgr.png" width="18" height="18"/> حسابات الإدارة <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/management">إدارة الحسابات</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">تغير كلمة المرور</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">صلاحيات</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">تقارير</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/car.png" width="18" height="18"/> السيارات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/cars">إضافة سيارة جديدة</a></li>
                                    <li><a href="../adminger/barnd">إضافة علامة تجارية جديدة - براند</a></li>
                                    <li><a href="../adminger/model"> موديل </a></li>
                                    <li><a href="../adminger/type"> أنواع السيارات </a></li>  
                                    <li><a href="../adminger/manifctor"> مصانع السيارات </a></li>                                 
                                </ul>
                            </li>
                            
                            <li><a><img src="../views/img/gift.png" width="18" height="18"/> قطع الغيار <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/parts">إضافة قطع غيار</a></li>
                                    <li><a href="../adminger/parts_type">أنواع القطع</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/product.png" width="18" height="18"/>  الخدمات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                <li><a href="../adminger/services_type"> أنواع الخدمات المتاحة</a></li>
                                <li><a href="../adminger/services"> الخدمات</a></li>
                                <li><a href="../adminger/technicians_Specfic"> تخصصات الفنيين  </a></li>
                                <li><a href="../adminger/technicians"> الفنيين  </a></li>
                                   <!-- <li><a href="../adminger/maintenance_workshops"> ورش صيانة</a></li>
                                   <li><a href="../adminger/accessories">مستلزمات السيارة </a></li>
                                    <li><a href="../adminger/compy_padding"> شركات التنجيد </a></li>
                                     -->
                                </ul>
                            </li>


                            <li><a><img src="../views/img/settings.png" width="18" height="18"/> الإعدادات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                <li><a href="../adminger/store"> المتجر </a></li>
                            <li><a href="../adminger/Purchases">  إضافة كمية </a></li> 
                            <li><a href="../adminger/haraj_type">  أنواع الحراج </a></li>
                            <li><a href="../adminger/haraj">  قائمة الحراج </a></li>
                            <li><a href="../adminger/delivery"> التوصيل </a></li>
                            <li><a href="../adminger/area">المنطقة</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="تنظیمات">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="تمام صفحه" onclick="toggleFullScreen();">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="قفل" class="lock_btn">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="خروج" href="../adminger/logout">
                    <img src="../views/img/logout.png" width="18" height="18"/>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav hidden-print">
        <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><img src="../views/img/menu.png" width="18" height="18"/></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o">الإشعارات</i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="../views/img/img.jpg" alt="..." class="img-circle profile_img" style="border-radius:50px;margin: 0px;width:48px;height:48px;margin-left: 10px;"></span>
                                        <span>
                          <span><b>أسم المستخدم</b></span>
                          <span class="time">قبل 4 دقائق</span>
                        </span>
                                        <span class="message">
                          قام بإضافة منتج تابع للمتجر المعني
                        </span>
                                    </a>
                                </li>
                                
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
        <!-- /header content -->
        
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3> الخدمات</h3>
            </div>

            <div class="title_right">
               
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
        
            <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>إضافة خدمة جديدة  
                            <small></small>
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><img src="../views/img/arrow.png" width="16" height="16"/></a></li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                    <form class="form-horizontal form-label-left" action="../services/new" method="POST" enctype="multipart/form-data">
                          <div class="item form-group">
                             <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="ser_title" class="form-control col-md-7 col-xs-12"
                                               data-validate-length-range="6" data-validate-words="2" name="ser_title"
                                                              placeholder="الخدمة" required="required" type="text">
                                                                  </div>
                          </div>
                         <div class="item form-group">
                               <div class="col-md-6 col-sm-6 col-xs-12">
                               <input id="ser_periods" class="form-control col-md-7 col-xs-12"
                                   data-validate-length-range="6" data-validate-words="2" name="ser_periods"
                                          placeholder="الفترة" required="required" type="text">
                                           </div>
                                </div>

                        <div class="item form-group">
                           <div class="col-md-6 col-sm-6 col-xs-12">
                               <input id="ser_address" class="form-control col-md-7 col-xs-12"
                                              data-validate-length-range="6" data-validate-words="2" name="ser_address"
                                                             placeholder="العنوان" required="required" type="text">
                                                                 </div>
                                                                 </div>

<div class="item form-group">
    
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="ser_latlang" id="ser_latlang" placeholder="الأحداثيات"
               data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12">
    </div>
</div>

<div class="item form-group">
    
    <div class="col-md-6 col-sm-6 col-xs-12">
        <textarea id="ser_details"  name="ser_details" placeholder="التفاصيل"
                  class="form-control col-md-7 col-xs-12"></textarea>
    </div>
</div>

<input type="hidden" name="m_status" value="2" />

<div class="item form-group">
    <div class="col-md-6 col-sm-6 col-xs-12">
<table class="tbl1">
    <tr>
       <td>
       <select name="ser_type" id="ser_type">
       <option value="0">النوع</option>
       <?php                               
          foreach($serv_typ as $val){
              echo '<option value="'.$val['sert_id'].'">'.$val['sert_type'].'</option>';
            }
       ?>

     </select>
     <br />

     </td>
     </tr>
</table>

</div>
</div>

<div class ="item form-group" >
  
</div>

<div class="item form-group">
    <div class="col-md-6 col-sm-6 col-xs-12">
<table class="tbl1">
    <tr>
       <td>
       <select name="ser_owner" id="owner">
       <option value="0">المعرض</option>
       <?php                               
          foreach($owner_store as $val){
              echo '<option value="'.$val['store_id'].'">'.$val['store_title'].'</option>';
            }
       ?>
        </select>
     <br />
     </td>
     </tr>
</table>

</div>
</div>


<!--<div class="item form-group">
   
    <div class="col-md-6 col-sm-6 col-xs-12">
        <textarea  required="required" name="ser_details" id="ser_details"
                  class="form-control col-md-7 col-xs-12"></textarea>
    </div>
</div>-->
<div class="item form-group">
    <div class="col-md-6 col-sm-6 col-xs-12">
      <table class="tbl1">
       <tr>
       <td>
       <select name="ser_country" id="country">
       <option value="0">المدينة</option>
            <?php                               
                      foreach($countery as $val){
                       echo '<option value="'.$val['c_id'].'">'.$val['c_name'].'</option>';
                        }
                ?>
          </select>
               <br />
               </td>
                  </tr>
    </table>
    </div>
</div>

<div class="item form-group">
    <div class="col-md-6 col-sm-6 col-xs-12">
    <table class="tbl1">
    <tr>
       <td>
       <select name="ser_area" id="marea">;
           <option value="0">أختر منطقة</option>
        </select>
     <br />
     </td>
     </tr>
</table>
</div>
</div>

<div class="item form-group">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="file" id="ser_thumb" name="ser_thumb" 
               required="required" class="form-control col-md-7 col-xs-12">
    </div>
</div>

<div class="ln_solid"></div>

<div class="form-group">
            <div class="col-md-6 col-md-offset-3" style="margin: 0;display: flex;">
                <button id="send" type="submit" class="btn btn-success" style="width:40%;margin:10px 0;">جديد</button>
                <button id="update" type="button" class="btn btn-success">حفظ</button>
                <button id="reset" type="button" class="btn btn-success">تراجع</button>
                <input type="hidden" id="ser_id" value=""/>
            </div>
        </div>

</form>
                    </div>
                </div>
            </div>



             <!-- Row 2 -->
            <div class="col-md-8 col-sm-8 col-xs-8">
            
            <div class="x_panel">
                    <div class="x_title">
                        <h2>الخدمات 
                           
                        <select name="stype" id="stype" style="border:1px solid #aaa;width:140px;font-size:12.5px;padding:4px 0;text-align:center;">;
                              <option value="0">أختر خدمة</option>
                                   <?php                               
                                      foreach($serv_typ as $val){
                                          echo '<option value="'.$val['sert_id'].'">'.$val['sert_type'].'</option>';
                                        }
                                   ?>
                            </select>

                            <button id="getServices" type="button" class="btn btn-success" style="width:80px;margin:10px 0;background-color:#116553;">عرض</button>
                        
                              
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><img src="../views/img/arrow.png" width="16" height="16"/></a></li>
                            <!--<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">تنظیمات 1</a>
                                    </li>
                                    <li><a href="#">تنظیمات 2</a>
                                    </li>
                                </ul>
                            </li>-->
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>المالك</th>
                                <th>النوع</th>
                                <th>تفاصيل الخدمة</th>
                                <th>تاريخ الإنشاء</th>
                                <th>تعديل</th>
                                <th>تحرير/حظر</th>
                            </tr>
                            </thead>
                            <tbody id="refresh">
                           
                                <?php
                               foreach($service as $val){
                                    echo'<tr><td scope="row"><input class="accID" name="accID" value="'.$val['ser_id'].'" type="radio"></td>
                                    <td>'.$val['owner'].'</td>
                                    <td>'.$val['stype'].'</td>
                                    <td>'.((strlen($val['ser_details'])>20)?mb_substr($val['ser_details'],0,20).'...':$val['ser_details']).'</td>
                                    <td>'.$val['ser_date'].'</td>
                                    <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/></td>
                                    <td>'.(($val['sert_status']==0)?'<img src="../views/img/release.png" width="24" height="24" class="release_btn"  style="cursor:pointer;"/>':'<img src="../views/img/stop.png" width="24" height="24" class="del_btn"  style="cursor:pointer;"/>').'</td></tr>';
                                }
                                ?>
                            
                            </tbody>
                        </table>

                    </div>
                </div>


             </div>



        </div>
    </div>
</div>
<!-- /page content -->
<!-- /page content -->

        <!-- footer content -->
        <footer class="hidden-print">
            <div class="pull-left">
            <!-- Gentelella - قالب پنل مدیریت بوت استرپ <a href="https://colorlib.com">Colorlib</a> | پارسی شده توسط <a
                    href="https://morteza-karimi.ir">مرتضی کریمی</a>-->
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
<div id="lock_screen">
    <table>
        <tr>
            <td>
                <div class="clock"></div>
                <span class="unlock">
                    <span class="fa-stack fa-5x">
                      <i class="fa fa-square-o fa-stack-2x fa-inverse"></i>
                      <i id="icon_lock" class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                </span>
            </td>
        </tr>
    </table>
</div>
<!-- jQuery -->
<script src="../views/js/jquery-3.5.1.min.js"></script>
<!-- Bootstrap -->
<script src="../views/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../views/js/fastclick.js"></script>
<!-- NProgress -->
<script src="../views/js/nprogress.js"></script>
<!-- bootstrap-progressbar -->
<script src="../views/js/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../views/js/icheck.min.js"></script>

<!-- bootstrap-daterangepicker -->
<script src="../views/js/moment.min.js"></script>

<script src="../views/js/daterangepicker.js"></script>

<!-- validator -->
<script src="../views/js/validator.js"></script>

<!-- Custom Theme Scripts -->
<script src="../views/js/custom.min.js"></script>
<script src="../views/js/jquery.alertable.min.js"></script>
<script>
   $(document).ready(function(){

     //get latlang..
     /*getLocation();

        function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else { 
            $.alertable.alert("Geolocation is not supported by this browser.");
        }
        }

        function showPosition(position) {
        $('#ser_latlang').val('{Latitude: ' + position.coords.latitude + 
        ',Longitude: ' + position.coords.longitude+'}');
        }*/


   $('#country').change(function(){
    $.ajax({
        url:'../adminger/add_area',
        type:'GET',
        data:{'c_id':$('#country option:selected').val()},
        beforeSend:function(){
        },
        success:function(area){
            $('#marea').children().remove();
            $('#marea').append(area);
        }
    })
})


var service;
    $(document).on('click','.edit_btn',function() {

        if($('.accID').is(':checked')){
            //$.alertable.alert($(this).parents('tr').find('td .accID').val());
            $.ajax({
                url:'../adminger/get_service',
                type:'GET',
                data:{'id':$(this).parents('tr').find('td .accID').val()},
                beforeSend:function(){},
                success:function(ser){
                
                   var service = JSON.parse(ser);
                   $('#ser_id').val(service.ser_id);
                   $('#ser_title').val(service.ser_title);
                   $('#ser_details').val(service.ser_details);
                   $('#ser_address').val(service.ser_address);
                   $('#ser_periods').val(service.ser_periods);
                   $('#ser_latlang').val(service.ser_latlang);
                 
                   $('#ser_type > option').each(function(){
                      if(this.value == service.ser_type)
                       $(this).attr('selected','selected');
                      else
                      $(this).removeAttr('selected');
                   });

                   $('#owner > option').each(function(){
                      if(this.value == service.ser_owner)
                       $(this).attr('selected','selected');
                      else
                      $(this).removeAttr('selected');
                   });

                   $('#country > option').each(function(){
                      if(this.value == service.ser_country)
                       $(this).attr('selected','selected');
                      else
                      $(this).removeAttr('selected');
                   });
                 
                   $.ajax({
                        url:'../adminger/add_area',
                        type:'GET',
                        data:{'c_id':$('#country option:selected').val()},
                        beforeSend:function(){
                        },
                        success:function(area){
                            $('#marea').children().remove();
                            $('#marea').append(area);

                            $('#marea > option').each(function(){
                                if(this.value == service.ser_area)
                                $(this).attr('selected','selected');
                                else
                                $(this).removeAttr('selected');
                            });

                            
                        }
                    })
                    
                   
                }
            })
            $('#update,#reset').css({'display':'block'});
            $('#send').css({'display':'none'});
        }else{
            $('.main_container').css({'filter':'blur(2.5px)'});
            $.alertable.alert('ارجاء إختر الخدمة').always(function(){
                $('.main_container').css({'filter':'blur(0)'});
            });
            $('.alertable').addClass('animate__animated animate__rubberBand');
        }
     });

    $(document).on('click','#reset',function() {
        $('input[type="text"],input[type="email"],textarea').val('');
        $('#update,#reset').css({'display':'none'});
            $('#send').css({'display':'block'});
    });


    $(document).on('click','#update',function() {

     $.alertable.confirm('هل أنت متأكد?').then(function() {

        $('.main_container').css({'filter':'blur(2.5px)'});

        $.ajax({
        url:'../adminger/service_update',
        type:'POST',
        data:{
            'ser_id':$('#ser_id').val(),
            'ser_title':$('#ser_title').val(),
            'ser_details':$('#ser_details').val(),
            'ser_address':$('#ser_address').val(),
            'ser_periods':$('#ser_periods').val(),
            'ser_latlang':$('#ser_latlang').val(),
            'ser_area':$('#marea option:selected').val(),
            'ser_country':$('#country option:selected').val(),
            'ser_owner':$('#owner option:selected').val(),
            'ser_type':$('#ser_type option:selected').val()
            },
        beforeSend:function(){

        },
        success:function(commited){
            $.alertable.alert(commited).always(function(){
                $('.main_container').css({'filter':'blur(0)'});
                                location.reload();
                            });
        }
       })

     },function(){
                $('.main_container').css({'filter':'blur(0)'});
            });

            $('.alertable').addClass('animate__animated animate__rubberBand');
    });


    $(document).on('click','.del_btn',function() {
        var sel='';
        if($('.accID').is(':checked')){
              
            sel=$(this).parents('tr').find('td .accID').val();  
            $('.main_container').css({'filter':'blur(2.5px)'});
           // $.alertable.alert($(this).parents('tr').find('td .accID').val()); 
            $.alertable.confirm('هل أنت متأكد?').then(function() {
               // $.alertable.alert(sel);

                    $.ajax({
                        url:'../adminger/services/block',
                        type:'GET',
                        data:{'id':sel},
                        beforeSend:function(){

                        },
                        success:function(blk){
                            $.alertable.alert(blk).always(function(){
                                location.reload();
                            });
                        }
                    });
            },function(){
                $('.main_container').css({'filter':'blur(0)'});
            });

            $('.alertable').addClass('animate__animated animate__rubberBand');

        }else{
            $('.main_container').css({'filter':'blur(2.5px)'});

            $.alertable.alert('الرجاء إختر خدمة').always(function(){
                $('.main_container').css({'filter':'blur(0)'});
            });
            $('.alertable').addClass('animate__animated animate__rubberBand');
        }
 });
 
       $(document).on('click','.release_btn',function() {

        var sel='';
        if($('.accID').is(':checked')){
            
            sel=$(this).parents('tr').find('td .accID').val();  
            $('.main_container').css({'filter':'blur(2.5px)'});
            //$.alertable.alert($(this).parents('tr').find('th .accID').val()); 
            $.alertable.confirm('هل أنت متأكد?').then(function() {
                //$.alertable.alert(sel);

                $.ajax({
                        url:'../adminger/services/release',
                        type:'GET',
                        data:{'id':sel},
                        beforeSend:function(){

                        },
                        success:function(res){
                            $.alertable.alert(res).always(function(){
                                location.reload();
                            });
                        }
                    });
            },function(){
                $('.main_container').css({'filter':'blur(0)'});
            });
            $('.alertable').addClass('animate__animated animate__rubberBand');
            
        }else{

            $('.main_container').css({'filter':'blur(2.5px)'});
            
            $.alertable.alert('الرجاء إختر خدمة').always(function(){
                $('.main_container').css({'filter':'blur(0)'});
            });

            $('.alertable').addClass('animate__animated animate__rubberBand');
        }

        });



 $('#getServices').click(function(){
    
    $.ajax({
        url:'../adminger/service/list',
        type:'GET',
        data:{'stype':$('#stype option:selected').val()},
        beforeSend:function(){

        },
        success:function(res){
            $('#refresh').children().remove();
            $('#refresh').append(res);
        }
    })
})


});
</script>
</body>
</html>
