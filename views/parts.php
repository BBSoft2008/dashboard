<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>

<style>

    #update,#reset{
           display: none;
       }
       .tbl1{
           /*margin:0 auto;*/
           margin-right: 15px;
       }
       .tbl1 select{
        width: 220px;
        border: 1px solid #aaa;
        height: 35px;
       }
       .tbl1 td{
           padding:3px 0px;
           font-weight:bold;
       }
       .form-horizontal .form-group{
           width: 500px;
       }
       .x_content .table td{
           font-size: 12px;
           vertical-align: bottom;
       }
    </style>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="fontiran.com:license" content="Y68A9">
    <link rel="icon" href="../build/images/favicon.ico" type="image/ico"/>
 

   <!-- Bootstrap -->
   <link href="../views/css/bootstrap.min.css" rel="stylesheet">
    <link href="../views/css/bootstrap-rtl.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../views/css/fontawesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../views/css/nprogress.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="../views/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../views/css/green.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../views/css/daterangepicker.css" rel="stylesheet">
    
    <link href='../views/css/jquery.alertable.css' rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../views/css/custom.min.css" rel="stylesheet">
    <link href="../views/css/animate.min.css" rel="stylesheet">
</head>
<!-- /header content -->
<body class="nav-md">
<div class="container body">
    <div class="main_container">
    <div class="col-md-3 left_col hidden-print">
            <div class="left_col scroll-view">
                <!--<div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
                </div>-->

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                    <img src="../views/img/img.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                    <span>مرحبا بك</span>
                        <h2><?= 'Admin '; ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br/>

                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>لوحة تحكم</h3>
                        <ul class="nav side-menu">
                            <li><a href="../adminger/"><img src="../views/img/home.png" width="18" height="18"/> الرئيسية </a>
                            </li>
                            <li><a><img src="../views/img/user.png" width="18" height="18"/> حسابات الأعضاء <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/accounts">إدارة الحسابات</a></li>
                                    <li><a href="../adminger/chang_pwd">تغير كلمة المرور</a></li>
                                  <!--  <li><a href="../adminger/chang_pwd">صلاحيات</a></li>
                                    <li><a href="../adminger/chang_pwd">إضافة مناطق</a></li>
                                    <li><a href="../adminger/chang_pwd">إضافة مدن</a></li>
                                    <li><a href="../adminger/chang_pwd">أنواع الحسابات</a></li> -->
                                    <li><a href="../adminger/chang_pwd">تقارير</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/mgr.png" width="18" height="18"/> حسابات الإدارة <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/management">إدارة الحسابات</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">تغير كلمة المرور</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">صلاحيات</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">تقارير</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/car.png" width="18" height="18"/> السيارات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/cars">إضافة سيارة جديدة</a></li>
                                    <li><a href="../adminger/barnd">إضافة علامة تجارية جديدة - براند</a></li>
                                    <li><a href="../adminger/model"> موديل </a></li>
                                    <li><a href="../adminger/type"> أنواع السيارات </a></li>  
                                    <li><a href="../adminger/manifctor"> مصانع السيارات </a></li>   
                                    <li><a href="../adminger/Purchases">  إضافة كمية </a></li>                              
                                </ul>
                            </li>
                            
                            <li><a><img src="../views/img/gift.png" width="18" height="18"/> قطع الغيار <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/parts">إضافة قطع غيار</a></li>
                                    <li><a href="../adminger/parts_type">أنواع القطع</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/product.png" width="18" height="18"/>  الخدمات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                <li><a href="../adminger/services_type"> أنواع الخدمات المتاحة</a></li>
                                <li><a href="../adminger/services"> الخدمات</a></li>
                                <li><a href="../adminger/technicians_Specfic"> تخصصات الفنيين  </a></li>
                                <!-- <li><a href="../adminger/technicians"> الفنيين  </a></li>-->
                                   <!-- <li><a href="../adminger/maintenance_workshops"> ورش صيانة</a></li>
                                   <li><a href="../adminger/accessories">مستلزمات السيارة </a></li>
                                    <li><a href="../adminger/compy_padding"> شركات التنجيد </a></li>
                                     -->
                                </ul>
                            </li>


                            <li><a><img src="../views/img/settings.png" width="18" height="18"/> الإعدادات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                <li><a href="../adminger/store"> المتجر </a></li>
                            <li><a href="../adminger/Purchases">  إضافة كمية </a></li> 
                            <li><a href="../adminger/haraj_type">  أنواع الحراج </a></li>
                            <li><a href="../adminger/haraj">  قائمة الحراج </a></li>
                            <li><a href="../adminger/delivery"> التوصيل </a></li>
                            <li><a href="../adminger/area">المنطقة</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="تنظیمات">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="تمام صفحه" onclick="toggleFullScreen();">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="قفل" class="lock_btn">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="خروج" href="../adminger/logout">
                    <img src="../views/img/logout.png" width="18" height="18"/>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav hidden-print">
        <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><img src="../views/img/menu.png" width="18" height="18"/></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o">الإشعارات</i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="../views/img/img.jpg" alt="..." class="img-circle profile_img" style="border-radius:50px;margin: 0px;width:48px;height:48px;margin-left: 10px;"></span>
                                        <span>
                          <span><b>أسم المستخدم</b></span>
                          <span class="time">قبل 4 دقائق</span>
                        </span>
                                        <span class="message">
                          قام بإضافة منتج تابع للمتجر المعني
                        </span>
                                    </a>
                                </li>
                                
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
        <!-- /header content -->
        
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>  قطع الغيار</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" >
                        <span class="input-group-btn">
                              <button class="btn btn-default" type="button">ابحث!</button>
                          </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>إضافة قطع غيار  
                           <!-- <small>مستخدم</small> -->
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><img src="../views/img/arrow.png" width="16" height="16"/></a>
                            </li>
                            <!--<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">تنظیمات 1</a>
                                    </li>
                                    <li><a href="#">تنظیمات 2</a>
                                    </li>
                                </ul>
                            </li>-->
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <form class="form-horizontal form-label-left" action="../parts/new" method="POST" enctype="multipart/form-data">
                          
                           

                            <div class="item form-group">
                                
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="name" class="form-control col-md-7 col-xs-12"
                                           data-validate-length-range="6" data-validate-words="2" name="pr_name"
                                           placeholder="اسم القطعة " required="required" type="text">
                                </div>
                             </div>

                           
                            
                            
                             <div class="item form-group">
                             <table class="tbl1">
                                <tr>
                                   <td>
                                 <select name="pr_mfr" id="pr_mfr"  required>;
                                   <?php     
                                      echo'<option value="0">المصنع</option>';                          
                                      foreach($manuf as $val){
                                          echo '<option value="'.$val['mfr_id'].'">'.$val['mfr_name'].'</option>';
                                        }
                                   ?>
                                 </select>
                                 </td>
                                 </tr>
                            </table>   
    
                            </div>

                            
                           <div class ="item form-group" >
                           <table class="tbl1">
                                <tr>
                                   <td>
                                   <select name="pr_brand" id="brand" required>;
                                   <?php     
                                   echo'<option value="0">البراند</option>';                           
                                      foreach($barnd as $val){
                                          echo '<option value="'.$val['b_id'].'">'.$val['b_title'].'</option>';
                                        }
                                   ?>

                                 </select>
                                 <br />

                                 </td>
                                 </tr>
                            </table>
   
                            </div>

                            
                            <div class ="item form-group" >
                            
                            <table class="tbl1">
                                <tr>
                                   <td>
                                   <select name="pr_model" id="model" required>;
                                       <option value="0">أختر الموديل</option>
                                    </select>
                                 <br />
                                 </td>
                                 </tr>
                            </table>


                            </div>

                            
                            <input type="hidden" name="is_sold" value="0" />
                          
                          
                            <div class="item form-group">
                            
                            <table class="tbl1">
                            <div class="item form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="vin_no" name="vin_no" required="required"
                                           data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" placeholder="رقم المركبة">
                            </div>
                        </div>
                             <tr>
                                   <td>
                                    <input id="id_new" name="is_new" class="typee" value="1" required="required" type="radio" checked>
                                    </td><td>جديدة</td>
                                  
                                   <td>
                                    <input id="id_used" name="is_new" class="typee" value="0" required="required" type="radio">
                                </td><td>مستخدم</td>
                                </tr>
                            </table>
                            
                               
                            </div>

                            
                        
                           
                            <div class="item form-group">
                               
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="pr_detailes" name="pr_detailes" placeholder="تفاصيل القطعة"
                                           required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>


                            <div class="item form-group">
                               
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" id="pr_img" name="pr_img[]" multiple accept="image/jpg, image/png, image/jpeg "
                                           required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>


                            <div class="item form-group">
                               
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea id="pr_comment"  name="pr_comment"
                                              class="form-control col-md-7 col-xs-12" placeholder="تعليق - ملاحظات"></textarea>
                                </div>
                            </div>

                            <div class ="item form-group" >
                        
                            <table class="tbl1">
                                <tr>
                                   <td>
                                <select name="pr_type" id="pr_type" required>
                                <?php                          
                                       echo '<option value="0">نوع القطعة</option>';     
                                       foreach($pr_typ as $val){
                                        echo '<option value="'.$val['prt_id'].'">'.$val['prt_title'].'</option>';
                                      }
                                   ?>
                                 </select>
                                 </td>
                                 </tr>
                            </table>
                                    </div>


                            <div class ="item form-group" >
                            <table class="tbl1">
                                <tr>
                                   <td>
                                <select name="pr_currency" id="pr_currency">
                                <?php                      
                                echo '<option value="0">العملة</option>';          
                                       foreach($curr as $val){
                                        echo '<option value="'.$val['cu_id'].'">'.$val['cu_currency'].'</option>';
                                      }
                                   ?>
                                 </select>
                                 </td>
                                 </tr>
                            </table>  
                            </div>

                            
                            <div class ="item form-group" >
                            <table class="tbl1">
                                <tr>
                                   <td>
                                   <select name="pr_store" id="pr_store" required>;
                                   <?php        
                                   echo '<option value="0">المعرض</option>';                        
                                      foreach($ownr_store as $val){
                                          echo '<option value="'.$val['store_id'].'">'.$val['store_title'].'</option>';
                                        }
                                   ?>
                                    </select>
                                 </td>
                                 </tr>
                            </table>
  
                            </div>

                            
                                 

                                 <div class="item form-group">
                                
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number"  name="pr_price" id="pr_price" required="required"
                                           class="form-control col-md-7 col-xs-12" placeholder="السعر">
                                </div>
                            </div>

                            <div class ="item form-group" >
                           
                            <table class="tbl1">
                                <tr>
                                   <td>
                                    <input id="is_available" class ="available" name="is_available" value="1" required="required" type="radio" checked>
                                    </td><td>متاح</td>
                                  
                                   <td>
                                    <input id="is_not_available" class ="available" name="is_available"  value="0" required="required" type="radio">
                                </td><td>غير متاح - معلق</td>
                                </tr>
                            </table>


                            </div>


                           

                            <input type="hidden" name="is_haraj" value="0" />

                            <table class="tbl1">
                            <div class ="item form-group" >
                                           
                            <table class="tbl1">
                                <tr>
                                  <td><input  id="is_orginal" name="is_orginal" calss = "orginal" value="1" required="required" type="radio" checked></td><td>اصلي</td>
                                  <td><input id="is_not_orginal" name="is_orginal"   calss = "orginal" value="0" required="required" type="radio"></td><td>تقليد</td>
                                </tr>
                            </table>

                            </div>


             
                            
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3" style="display:flex;">
                                    <button id="send" type="submit" class="btn btn-success">جديد</button>
                                    <button id="update" type="button" class="btn btn-success">حفظ</button>
                                    <button id="reset" type="button" class="btn btn-success">تراجع</button>
                                    <input type="hidden" id="pr_id" value=""/>
                                </div>
                            </div>
                        </form>
                    </div>


                    
                </div>


                <!-- Row 2 -->
          <div class="col-md-8 col-sm-8 col-xs-8">
            
            <div class="x_panel">
                    <div class="x_title">
                        <h2>إدارة قطع الغيار
                           
                                <input type="text" id="search" style="border:1px solid #aaa;width:140px;font-size:12.5px;padding:4px 0;text-align:center;" placeholder="بحث"/>
                                
                                  <!--  <select style="width:100px;font-size:12.5px;border:1px solid #aaa;">
                                        <option value="0">نوع الحساب</option>
                                        <?php                               
                                        /* foreach($types as $val){
                                          echo '<option value="'.$val['ut_id'].'">'.$val['ut_type'].'</option>';
                                        }*/
                                   ?>
                                    </select>-->
                              
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><img src="../views/img/arrow.png" width="16" height="16"/></a></li>
                            <!--<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">تنظیمات 1</a>
                                    </li>
                                    <li><a href="#">تنظیمات 2</a>
                                    </li>
                                </ul>
                            </li>-->
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>أسم القطعة</th>
                                <th>البراند</th>
                                <th>تاريخ الإنشاء</th>
                                <th>تعديل</th>
                                <th>حذف</th>
                            </tr>
                            </thead>
                            <tbody id="refresh">
                           
                                <?php
                                //var_dump($cars);
                                foreach($parts as $val){
                                    echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['pr_id'].'" type="radio"></th>
                                    <td>'.$val['pr_name'].'</td>
                                    <td>'.$val['brand'].'</td>
                                    <td>'.$val['pr_cdate'].'</td>
                                    <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/></td>
                                    <td>'.(($val['is_available']==0)?'<img src="../views/img/release.png" width="24" height="24" class="release_btn"  style="cursor:pointer;"/>':'<img src="../views/img/stop.png" width="24" height="24" class="del_btn"  style="cursor:pointer;"/>').'</td></tr>';
                                }
                                ?>
                            
                            </tbody>
                        </table>

                    </div>
                </div>


             </div>

                
            </div>



            
            

        </div>
 
        
        
 
    </div>

    
</div>
<!-- /page content -->
<!-- /page content -->

        <!-- footer content -->
        <footer class="hidden-print">
            <div class="pull-left">
            <!-- Gentelella - قالب پنل مدیریت بوت استرپ <a href="https://colorlib.com">Colorlib</a> | پارسی شده توسط <a
                    href="https://morteza-karimi.ir">مرتضی کریمی</a>-->
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
<div id="lock_screen">
    <table>
        <tr>
            <td>
                <div class="clock"></div>
                <span class="unlock">
                    <span class="fa-stack fa-5x">
                      <i class="fa fa-square-o fa-stack-2x fa-inverse"></i>
                      <i id="icon_lock" class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                </span>
            </td>
        </tr>
    </table>
</div>
<!-- jQuery -->
<script src="../views/js/jquery-3.5.1.min.js"></script>
<!-- Bootstrap -->
<script src="../views/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../views/js/fastclick.js"></script>
<!-- NProgress -->
<script src="../views/js/nprogress.js"></script>
<!-- bootstrap-progressbar -->
<script src="../views/js/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../views/js/icheck.min.js"></script>

<!-- bootstrap-daterangepicker -->
<script src="../views/js/moment.min.js"></script>

<script src="../views/js/daterangepicker.js"></script>

<!-- validator -->
<script src="../views/js/validator.js"></script>

<!-- Custom Theme Scripts -->
<script src="../views/js/custom.min.js"></script>

<script src="../views/js/jquery.alertable.min.js"></script>

<script>

$(document).ready(function(){
    var car;
$(document).on('click','.edit_btn',function() {

   if($('.accID').is(':checked')){
    //$.alertable.alert($(this).parents('tr').find('th .accID').val());
    $.ajax({
                url:'../adminger/parts/modify',
                type:'GET',
                data:{'id':$(this).parents('tr').find('th .accID').val()},
                beforeSend:function(){},
                success:function(user){
                   var car = JSON.parse(user)[0];
                   $('#pr_id').val(car.pr_id);
                   $('#name').val(car.pr_name);
                  

                    //get models based on brand id.

                    $.ajax({
                    url:'../adminger/models',
                    type:'GET',
                    data:{'brand_id':car.pr_brand},
                    beforeSend:function(){},
                    success:function(models){
                     $('#model').children().remove();
                     $('#model').append(models)

                        $('#model > option').each(function(){
                            if(this.value == car.pr_model)
                            $(this).attr('selected','selected');
                            else
                            $(this).removeAttr('selected');
                       });

                    }
               });


                   $('#vin_no').val(car.vin_no);
                   if(car.is_new=='1'){
                       $('#id_new').prop('checked',true);
                       $('#id_used').prop('checked',false);
                   }else if(car.is_new=='0'){
                       $('#id_new').prop('checked',false);
                       $('#id_used').prop('checked',true);
                   }
                   $('#pr_mfr > option').each(function(){
                      if(this.value == car.pr_mfr)
                       $(this).attr('selected','selected');
                      else
                      $(this).removeAttr('selected');
                   });

                   $('#pr_detailes').val(car.pr_detailes);
                   $('#pr_comment').val(car.pr_comment);
                  // $('#p_cdate').val(car.p_cdate);
                  $('#brand > option').each(function(){
                      if(this.value == car.pr_brand)
                       $(this).attr('selected','selected');
                      else
                      $(this).removeAttr('selected');
                   });
                   $('#pr_currency > option').each(function(){
                      if(this.value == car.pr_currency)
                       $(this).attr('selected','selected');
                      else
                      $(this).removeAttr('selected');
                   });
                   $('#pr_price').val(car.pr_price);
                  // $('#pr_img').val(car.pr_price);
                  if(car.is_available=='1'){
                       $('#is_available').prop('checked',true);
                       $('#is_not_available').prop('checked',false);
                   }else if(car.is_available=='0'){
                       $('#is_available').prop('checked',false);
                       $('#is_not_available').prop('checked',true);
                   }
                  // $('#is_haraj').val(car.is_haraj);
                  $('#pr_type > option').each(function(){
                      if(this.value == car.pr_type)
                       $(this).attr('selected','selected');
                      else
                      $(this).removeAttr('selected');
                   });
                   if(car.is_orginal=='1'){
                       $('#is_orginal').prop('checked',true);
                       $('#is_not_orginal').prop('checked',false);
                   }else if(car.is_orginal=='0'){
                       $('#is_orginal').prop('checked',false);
                       $('#is_not_orginal').prop('checked',true);
                   }

                   $('#pr_store > option').each(function(){
                      if(this.value == car.pr_store)
                       $(this).attr('selected','selected');
                      else
                      $(this).removeAttr('selected');
                   });
                }
            })
            $('#update,#reset').css({'display':'block'});
            $('#send').css({'display':'none'});
            }else{
                $('.main_container').css({'filter':'blur(2.5px)'});
            $.alertable.alert('ارجاء إختر قطعة غيار').always(function(){
                $('.main_container').css({'filter':'blur(0)'});
            });
            $('.alertable').addClass('animate__animated animate__rubberBand');
             }
     });

$(document).on('click','#reset',function() {
        $('input[type="text"],input[type="email"],textarea').val('');
        $('#update,#reset').css({'display':'none'});
            $('#send').css({'display':'block'});
    });

$(document).on('click','#update',function() {

    $('.main_container').css({'filter':'blur(2.5px)'});
       $.alertable.confirm('هل أنت متأكد?').then(function() { 
           
            $.ajax({
                   url:'../adminger/parts/update',
                      type:'POST',
                         data:{
                                'pr_id':$('#pr_id').val(),
                                'pr_name':$('#name').val(),
                                //'is_sold':$('#is_sold').val(),
                                'pr_model':$('#model option:selected').val(),
                                'vin_no':$('#vin_no').val(),
                                'is_new':$('.typee:checked').val(),
                                'pr_mfr':$('#pr_mfr option:selected').val(),
                                'pr_detailes':$('#pr_detailes').val(),
                                'pr_comment':$('#pr_comment').val(),
                                'pr_brand':$('#brand option:selected').val(),
                                'pr_currency':$('#pr_currency option:selected').val(),
                                'pr_price':$('#pr_price').val(),
                                //'pr_img':$('#pr_img').val(),
                                'is_available':$('.available:checked').val(),
                               // 'is_haraj':$('is_haraj:checked').val(),
                                'pr_type':$('#pr_type option:selected').val(),
                                'orginal':$('#is_orginal:checked').val()?$('#is_orginal:checked').val():$('#is_not_orginal:checked').val(),
                                'pr_store':$('#pr_store option:selected').val(),
                                 },
                                 beforeSend:function(){
                                 },
                                 success:function(commited){
                                     $.alertable.alert(commited);
                                }
                        })
                      },function(){
                $('.main_container').css({'filter':'blur(0)'});
               });
               $('.alertable').addClass('animate__animated animate__rubberBand');
            });

$(document).on('click','.del_btn',function() {
 var sel='';
  if($('.accID').is(':checked')){
      
    sel=$(this).parents('tr').find('th .accID').val();  
    $('.main_container').css({'filter':'blur(2.5px)'});
    //$.alertable.alert($(this).parents('tr').find('th .accID').val()); 
    $.alertable.confirm('هل أنت متأكد?').then(function() {
       // $.alertable.alert(sel); 
       $.ajax({
                url:'../adminger/parts/block',
                type:'GET',
                data:{'id':sel},
                beforeSend:function(){
                },
                success:function(blk){
                 $.alertable.alert(blk).always(function(){
                 location.reload();
                 });
            }
        });
    },function(){
                $('.main_container').css({'filter':'blur(0)'});
            });
            $('.alertable').addClass('animate__animated animate__rubberBand');

  }else{
      
    $('.main_container').css({'filter':'blur(2.5px)'});
            $.alertable.alert('الرجاء إختر قطعة غيار').always(function(){
                $('.main_container').css({'filter':'blur(0)'});
            });
            $('.alertable').addClass('animate__animated animate__rubberBand');
  }
  });

$(document).on('click','.release_btn',function() {
 var sel='';
  if($('.accID').is(':checked')){
    
    sel=$(this).parents('tr').find('th .accID').val();  
    $('.main_container').css({'filter':'blur(2.5px)'});
    //$.alertable.alert($(this).parents('tr').find('th .accID').val()); 
    $.alertable.confirm('هل أنت متأكد?').then(function() {
        //$.alertable.alert(sel);

        $.ajax({
                url:'../adminger/product/release',
                type:'GET',
                data:{'id':sel},
                beforeSend:function(){
                },
                success:function(res){
                    $.alertable.alert(res).always(function(){
                        location.reload();
                    });
                }
            });
    },function(){
                $('.main_container').css({'filter':'blur(0)'});
            });
            $('.alertable').addClass('animate__animated animate__rubberBand');
 } else{
    $('.main_container').css({'filter':'blur(2.5px)'});
    $.alertable.alert('الرجاء إختر قطعة غيار').always(function(){
                $('.main_container').css({'filter':'blur(0)'});
            });

            $('.alertable').addClass('animate__animated animate__rubberBand');
 }

 });








  $('#brand').change(function(){
    
    $.ajax({
        url:'../adminger/add_model',
        type:'GET',
        data:{'b_id':$('#brand option:selected').val()},
        beforeSend:function(){

        },
        success:function(model){
            $('#model').children().remove();
            $('#model').append(model);
        }
     })
   } )






  $('#search').keyup(function(){

     $.ajax({
         url:'../adminger/parts/search',
         type:'GET',
         data:{'word':$('#search').val()},
         beforeSend:function(){
         },
         success:function(res){
                 $('#refresh').children().remove();
                 $('#refresh').append(res);
                 }
                })
            })
});

</script>

</body>
</html>
