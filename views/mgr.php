<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="fontiran.com:license" content="Y68A9">
    <link rel="icon" href="../build/images/favicon.ico" type="image/ico"/>
 

   <!-- Bootstrap -->
   <link href="../views/css/bootstrap.min.css" rel="stylesheet">
    <link href="../views/css/bootstrap-rtl.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../views/css/fontawesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../views/css/nprogress.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="../views/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../views/css/green.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../views/css/daterangepicker.css" rel="stylesheet">
    <link href='../views/css/jquery.alertable.css' rel="stylesheet">
    <link href="../views/css/animate.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../views/css/custom.min.css" rel="stylesheet">
    <style>

       #update,#reset{
           display: none;
       }

       .tbl1{
           /*margin:0 auto;*/
       }
       .tbl1 select{
        width: 220px;
        border: 1px solid #aaa;
       }
       .tbl1 td{
           padding:3px 0px;
           font-weight:bold;
       }
       .form-horizontal .form-group{
           width: 500px;
       }
       .x_content .table td{
           font-size: 12px;
           vertical-align: bottom;
       }
    </style>
</head>
<!-- /header content -->
<body class="nav-md">
<div class="container body">
    <div class="main_container">
    <div class="col-md-3 left_col hidden-print">
            <div class="left_col scroll-view">
                <!--<div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
                </div>-->

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                    <img src="../views/img/img.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                    <span>مرحبا بك</span>
                        <h2><?= 'Admin '; ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br/>

                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>لوحة تحكم</h3>
                        <ul class="nav side-menu">
                            <li><a href="../adminger/"><img src="../views/img/home.png" width="18" height="18"/> الرئيسية </a>
                            </li>
                            <li><a><img src="../views/img/user.png" width="18" height="18"/> حسابات الأعضاء <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/accounts">إدارة الحسابات</a></li>
                                    <li><a href="../adminger/chang_pwd">تغير كلمة المرور</a></li>
                                  <!--  <li><a href="../adminger/chang_pwd">صلاحيات</a></li>
                                    <li><a href="../adminger/chang_pwd">إضافة مناطق</a></li>
                                    <li><a href="../adminger/chang_pwd">إضافة مدن</a></li>
                                    <li><a href="../adminger/chang_pwd">أنواع الحسابات</a></li> -->
                                    <li><a href="../adminger/chang_pwd">تقارير</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/mgr.png" width="18" height="18"/> حسابات الإدارة <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/management">إدارة الحسابات</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">تغير كلمة المرور</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">صلاحيات</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">تقارير</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/car.png" width="18" height="18"/> السيارات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/cars">إضافة سيارة جديدة</a></li>
                                    <li><a href="../adminger/barnd">إضافة علامة تجارية جديدة - براند</a></li>
                                    <li><a href="../adminger/model"> موديل </a></li>
                                    <!--<li><a href="../adminger/type"> أنواع السيارات </a></li>-->  
                                    <li><a href="../adminger/manifctor"> مصانع السيارات </a></li>                                 
                                </ul>
                            </li>
                            
                            <li><a><img src="../views/img/gift.png" width="18" height="18"/> قطع الغيار <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/parts">إضافة قطع غيار</a></li>
                                    <li><a href="../adminger/parts_type">أنواع القطع</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/product.png" width="18" height="18"/>  الخدمات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                <li><a href="../adminger/services_type"> أنواع الخدمات المتاحة</a></li>
                                <li><a href="../adminger/services"> الخدمات</a></li>
                                <li><a href="../adminger/technicians_Specfic"> تخصصات الفنيين  </a></li>
                                <li><a href="../adminger/technicians"> الفنيين  </a></li>
                                   <!-- <li><a href="../adminger/maintenance_workshops"> ورش صيانة</a></li>
                                   <li><a href="../adminger/accessories">مستلزمات السيارة </a></li>
                                    <li><a href="../adminger/compy_padding"> شركات التنجيد </a></li>
                                     -->
                                </ul>
                            </li>


                            <li><a><img src="../views/img/settings.png" width="18" height="18"/> الإعدادات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                <li><a href="../adminger/store"> المتجر </a></li>
                            <li><a href="../adminger/Purchases">  إضافة كمية </a></li> 
                            <li><a href="../adminger/haraj_type">  أنواع الحراج </a></li>
                            <li><a href="../adminger/haraj">  قائمة الحراج </a></li>
                            <li><a href="../adminger/delivery"> التوصيل </a></li>
                            <li><a href="../adminger/area">المنطقة</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="تنظیمات">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="تمام صفحه" onclick="toggleFullScreen();">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="قفل" class="lock_btn">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="خروج" href="../adminger/logout">
                    <img src="../views/img/logout.png" width="18" height="18"/>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav hidden-print">
        <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><img src="../views/img/menu.png" width="18" height="18"/></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o">الإشعارات</i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="../views/img/img.jpg" alt="..." class="img-circle profile_img" style="border-radius:50px;margin: 0px;width:48px;height:48px;margin-left: 10px;"></span>
                                        <span>
                          <span><b>أسم المستخدم</b></span>
                          <span class="time">قبل 4 دقائق</span>
                        </span>
                                        <span class="message">
                          قام بإضافة منتج تابع للمتجر المعني
                        </span>
                                    </a>
                                </li>
                                
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
        <!-- /header content -->
        
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3> حسابات المستخدمين</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" >
                        <span class="input-group-btn">
                              <button class="btn btn-default" type="button">ابحث!</button>
                          </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
        
            <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>انشاء حساب جديد  
                            <small></small>
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><img src="../views/img/arrow.png" width="16" height="16"/></a></li>
                            <!--<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">تنظیمات 1</a>
                                    </li>
                                    <li><a href="#">تنظیمات 2</a>
                                    </li>
                                </ul>
                            </li>-->
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                    <form class="form-horizontal form-label-left" action="../mgr/new" method="POST" enctype="multipart/form-data">

                           <!-- <p>برای کتابخانه جایگزین برای اعتبار سنجی در صفحه فرم عمومی <code>parsleyJS</code> را نگاه کنید <a
                                    href="form.html">فرم عمومی</a>
                            </p> -->
                           <br /><br />


                        

                            <div class="item form-group">
                                
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="fname" class="form-control col-md-7 col-xs-12"
                                           data-validate-length-range="6" data-validate-words="2" name="u_fname"
                                           placeholder="الاسم الأول" required="required" type="text">
                                </div>
                            </div>

                            <div class="item form-group">
                               
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="lname" class="form-control col-md-7 col-xs-12"
                                           data-validate-length-range="6" data-validate-words="2" name="u_lname"
                                           placeholder="الاسم الثاني" required="required" type="text">
                                </div>
                            </div>
                           
        


                            <div class="item form-group">
                                
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                
                                <table class="tbl1">
                                <tr>
                                   <td>
                                    <input id="male" class="gender" name="u_gender" placeholder="ذكر " value="m" required="required" type="radio" checked>
                                    </td><td>ذكر</td>
                                     <td>&nbsp;&nbsp;</td>
                                   <td>
                                    <input id="female"  class="gender" name="u_gender"
                                           placeholder="ذكر " required="required" type="radio" value="f">
                                </td><td>أنثى</td>
                                
                                
                                </tr>
                                </table>
                                       
                            
                            </div>
                            </div>
                           

                            <div class="item form-group">
                               
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="u_account" name="u_account" required="required"
                                           data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" placeholder="الأسم الكامل">
                                </div>
                            </div>

                            <div class="item form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" id="email" name="u_email" required="required"
                                           class="form-control col-md-7 col-xs-12" placeholder="الأيميل">
                                </div>
                            </div>
                            
                            <input type="hidden" name="u_status" value="2" />


                            <div class="item form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    
                                <table class="tbl1">
                                <tr>
                                   <td>
                                   <select name="u_utype" id="u_utype">;
                                   <?php                               
                                      foreach($types as $val){
                                          echo '<option value="'.$val['ut_id'].'">'.$val['ut_type'].'</option>';
                                        }
                                   ?>

                                 </select>
                                 <br />

                                 </td>
                                 </tr>
                            </table>

                            </div>
                            </div>


                            <div class="item form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="u_pwd" type="password" name="u_pwd" required="required"
                                           class="form-control col-md-7 col-xs-12" placeholder="كلمة المرور">
                                </div>
                            </div>
                           
                            <div class="item form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="re_u_pwd" type="password" name="re_u_pwd" required="required"
                                           class="form-control col-md-7 col-xs-12" placeholder=" تأكيد كلمة المرور">
                                </div>
                            </div>
                           
                           
                            <div class="item form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="phone" name="u_mobile" id="mobile"
                                           data-validate-length-range="5,20"
                                           class="optional form-control col-md-7 col-xs-12" required placeholder="الموبايل">
                                </div>
                            </div>
                           

                            <div class="item form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                 
                                <table class="tbl1">
                                <tr>
                                   <td>
                                   <select name="u_country" id="country">;
                                   <?php                               
                                      foreach($countery as $val){
                                          echo '<option value="'.$val['c_id'].'">'.$val['c_name'].'</option>';
                                        }
                                   ?>

                                 </select>
                                 <br />

                                 </td>
                                 </tr>
                            </table>

                            </div>
                            </div>
                           

                            <div class="item form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    
                                
                            <table class="tbl1">
                                <tr>
                                   <td>
                                   <select name="u_area" id="marea">;
                                       echo'<option value="0">أختر منطقة</option>';
                                    </select>
                                 <br />
                                 </td>
                                 </tr>
                            </table>

                                </div>
                            </div>
                          
                            

                            <div class="item form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="maddress" type="text" name="u_address" data-validate-length="6,8"
                                           class="form-control col-md-7 col-xs-12" required="required" placeholder="العنوان">
                                </div>
                            </div>

                            <div class="item form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea id="u_comment" required="required" name="u_comment"
                                              class="form-control col-md-7 col-xs-12" placeholder="الملاحظات"></textarea>
                                </div>
                            </div>


                            <div class="item form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" id="u_img" name="u_img" 
                                           required="required" class="form-control col-md-7 col-xs-12" placeholder="إرفاق صورة">
                                </div>
                            </div>
                           
                            <div class="ln_solid"></div>
                            
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3" style="margin: 0;display: flex;">
                                    <button id="send" type="submit" class="btn btn-success">جديد</button>
                                    <button id="update" type="button" class="btn btn-success">حفظ</button>
                                    <button id="reset" type="button" class="btn btn-success">تراجع</button>
                                    <input type="hidden" id="u_id" value=""/>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>



             <!-- Row 2 -->
            <div class="col-md-8 col-sm-8 col-xs-8">
            
            <div class="x_panel">
                    <div class="x_title">
                        <h2>إدارة حسابات
                           
                                <input type="text" id="search" style="border:1px solid #aaa;width:140px;font-size:12.5px;padding:4px 0;text-align:center;" placeholder="بحث"/>
                                
                                    <select style="width:100px;font-size:12.5px;border:1px solid #aaa;">
                                        <option value="0">نوع الحساب</option>
                                        <?php                               
                                      foreach($types as $val){
                                          echo '<option value="'.$val['ut_id'].'">'.$val['ut_type'].'</option>';
                                        }
                                   ?>
                                    </select>
                              
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><img src="../views/img/arrow.png" width="16" height="16"/></a></li>
                            <!--<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">تنظیمات 1</a>
                                    </li>
                                    <li><a href="#">تنظیمات 2</a>
                                    </li>
                                </ul>
                            </li>-->
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>الحساب</th>
                                <th>تاريخ الإنشاء</th>
                                <th>تعديل</th>
                                <th>تحرير/حظر</th>
                            </tr>
                            </thead>
                            <tbody id="refresh">
                           
                                <?php
                                foreach($accounts as $val){
                                    echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['u_id'].'" type="radio"></th>
                                    <td>'.$val['u_account'].'</td>
                                    <td>'.$val['u_cdate'].'</td>
                                    <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/></td>
                                    <td>'.(($val['u_status']==3)?'<img src="../views/img/release.png" width="24" height="24" class="release_btn"  style="cursor:pointer;"/>':'<img src="../views/img/stop.png" width="24" height="24" class="del_btn"  style="cursor:pointer;"/>').'</td></tr>';
                                }
                                ?>
                            
                            </tbody>
                        </table>

                    </div>
                </div>


             </div>



        </div>
    </div>
</div>
<!-- /page content -->
<!-- /page content -->

        <!-- footer content -->
        <footer class="hidden-print">
            <div class="pull-left">
            <!-- Gentelella - قالب پنل مدیریت بوت استرپ <a href="https://colorlib.com">Colorlib</a> | پارسی شده توسط <a
                    href="https://morteza-karimi.ir">مرتضی کریمی</a>-->
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
<div id="lock_screen">
    <table>
        <tr>
            <td>
                <div class="clock"></div>
                <span class="unlock">
                    <span class="fa-stack fa-5x">
                      <i class="fa fa-square-o fa-stack-2x fa-inverse"></i>
                      <i id="icon_lock" class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                </span>
            </td>
        </tr>
    </table>
</div>
<!-- jQuery -->
<script src="../views/js/jquery-3.5.1.min.js"></script>
<!-- Bootstrap -->
<script src="../views/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../views/js/fastclick.js"></script>
<!-- NProgress -->
<script src="../views/js/nprogress.js"></script>
<!-- bootstrap-progressbar -->
<script src="../views/js/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../views/js/icheck.min.js"></script>

<!-- bootstrap-daterangepicker -->
<script src="../views/js/moment.min.js"></script>

<script src="../views/js/daterangepicker.js"></script>

<!-- validator -->
<script src="../views/js/validator.js"></script>

<!-- Custom Theme Scripts -->
<script src="../views/js/custom.min.js"></script>
<script src="../views/js/jquery.alertable.min.js"></script>
<script>

$(document).ready(function(){

    var member;
    $(document).on('click','.edit_btn',function() {

        if($('.accID').is(':checked')){
            //$.alertable.alert($(this).parents('tr').find('th .accID').val());
            $.ajax({
                url:'../adminger/edit_mgr',
                type:'GET',
                data:{'id':$(this).parents('tr').find('th .accID').val()},
                beforeSend:function(){},
                success:function(user){
                   var member = JSON.parse(user)[0];
                   $('#u_id').val(member.u_id);
                   $('#fname').val(member.u_fname);
                   $('#lname').val(member.u_lname);
                   $('#u_account').val(member.u_account);
                   $('#mobile').val(member.u_mobile);
                   $('#email').val(member.u_email);
                   $('#maddress').val(member.u_address);
                   $('#u_comment').val(member.u_comment);
                  
                   
                   $('#country > option').each(function(){
                      if(this.value == member.u_country)
                       $(this).attr('selected','selected');
                      else
                      $(this).removeAttr('selected');
                   });

                   $('#u_utype > option').each(function(){
                      if(this.value == member.u_utype)
                       $(this).attr('selected','selected');
                      else
                      $(this).removeAttr('selected');
                   });

                   if(member.u_gender=='m'){
                       $('#male').prop('checked',true);
                       $('#female').prop('checked',false);
                   }else if(member.u_gender=='f'){
                       $('#male').prop('checked',false);
                       $('#female').prop('checked',true);
                   }

                   
                }
            })
            $('#update,#reset').css({'display':'block'});
            $('#send').css({'display':'none'});
        }else{
            $('.main_container').css({'filter':'blur(2.5px)'});
            $.alertable.alert('ارجاء إختر حساب مستخدم').always(function(){
                $('.main_container').css({'filter':'blur(0)'});
            });
            $('.alertable').addClass('animate__animated animate__rubberBand');
        }
       
    });

    $(document).on('click','#reset',function() {
        $('input[type="text"],input[type="email"],textarea').val('');
        $('#update,#reset').css({'display':'none'});
            $('#send').css({'display':'block'});

    });


    $(document).on('click','#update',function() {

        $('.main_container').css({'filter':'blur(2.5px)'});

     $.alertable.confirm('هل أنت متأكد?').then(function() {
              
        $.ajax({
        url:'../adminger/mgr/update',
        type:'POST',
        data:{
            'u_id':$('#u_id').val(),
            'fname':$('#fname').val(),
            'lname':$('#lname').val(),
            'u_account':$('#u_account').val(),
            'mobile':$('#mobile').val(),
            'email':$('#email').val(),
            'maddress':$('#maddress').val(),
            'u_comment':$('#u_comment').val(),
            'gender':$('.gender:checked').val(),
            'country':$('#country option:selected').val(),
            'u_utype':$('#u_utype option:selected').val()
            },
        beforeSend:function(){

        },
        success:function(commited){
            $.alertable.alert(commited).always(function(){
                $('.main_container').css({'filter':'blur(0)'});
                                location.reload();
                            });
        }
       },function(){
                $('.main_container').css({'filter':'blur(0)'});
            });

            $('.alertable').addClass('animate__animated animate__rubberBand');

     },function(){
                $('.main_container').css({'filter':'blur(0)'});
            });

            $('.alertable').addClass('animate__animated animate__rubberBand');


    });


    $(document).on('click','.del_btn',function() {

        var sel='';
        if($('.accID').is(':checked')){
              
            sel=$(this).parents('tr').find('th .accID').val();  
            $('.main_container').css({'filter':'blur(2.5px)'});
            //$.alertable.alert($(this).parents('tr').find('th .accID').val()); 
            $.alertable.confirm('هل أنت متأكد?').then(function() {
               // $.alertable.alert(sel);

                    $.ajax({
                        url:'../adminger/mgr/block',
                        type:'GET',
                        data:{'id':sel},
                        beforeSend:function(){

                        },
                        success:function(blk){
                            $.alertable.alert(blk).always(function(){
                                location.reload();
                            });
                        }
                    });

            },function(){
                $('.main_container').css({'filter':'blur(0)'});
            });
            
            $('.alertable').addClass('animate__animated animate__rubberBand');
            
        }else{
            $('.main_container').css({'filter':'blur(2.5px)'});
            $.alertable.alert('الرجاء إختر حساب مستخدم').always(function(){
                $('.main_container').css({'filter':'blur(0)'});
            });
            $('.alertable').addClass('animate__animated animate__rubberBand');
        }

      });


      $(document).on('click','.release_btn',function() {

        var sel='';
        if($('.accID').is(':checked')){
            
            sel=$(this).parents('tr').find('th .accID').val();  
            $('.main_container').css({'filter':'blur(2.5px)'});
            //$.alertable.alert($(this).parents('tr').find('th .accID').val()); 
            $.alertable.confirm('هل أنت متأكد?').then(function() {
                //$.alertable.alert(sel);

                $.ajax({
                        url:'../adminger/mgr/release',
                        type:'GET',
                        data:{'id':sel},
                        beforeSend:function(){

                        },
                        success:function(res){
                            $.alertable.alert(res).always(function(){
                                location.reload();
                            });
                        }
                    });
                    
            },function(){
                $('.main_container').css({'filter':'blur(0)'});
            });
            $('.alertable').addClass('animate__animated animate__rubberBand');
            
        }else{
            $('.main_container').css({'filter':'blur(2.5px)'});

            $.alertable.alert('الرجاء إختر حساب المستخدم').always(function(){
                $('.main_container').css({'filter':'blur(0)'});
            });

            $('.alertable').addClass('animate__animated animate__rubberBand');
        }

        });


    $('#search').keyup(function(){
    
    $.ajax({
        url:'../adminger/mgr/search',
        type:'GET',
        data:{'word':$('#search').val()},
        beforeSend:function(){

        },
        success:function(res){
            $('#refresh').children().remove();
            $('#refresh').append(res);
        }
    })
})



$('#country').change(function(){
    
    $.ajax({
        url:'../adminger/add_area',
        type:'GET',
        data:{'c_id':$('#country option:selected').val()},
        beforeSend:function(){

        },
        success:function(area){
            $('#marea').children().remove();
            $('#marea').append(area);
        }
    })
})

});

</script>
</body>
</html>
