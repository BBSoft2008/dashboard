<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="fontiran.com:license" content="Y68A9">
    <link rel="icon" href="../views/img/favicon.ico" type="image/ico"/>

    <title>أبو خالد للتجارة</title>

    <!-- Bootstrap -->
    <link href="../views/css/bootstrap.min.css" rel="stylesheet">
    <link href="../views/css/bootstrap-rtl.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../views/css/fontawesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../views/css/nprogress.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="../views/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../views/css/green.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../views/css/daterangepicker.css" rel="stylesheet">
    <link href="../views/css/animate.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../views/css/custom.min.css" rel="stylesheet">
<style>
    .legend{
        display: none;
    }
</style>
</head>
<!-- /header content -->
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col hidden-print">
            <div class="left_col scroll-view">
                <!--<div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
                </div>-->

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                    <img src="../views/img/img.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                    <span>مرحبا بك</span>
                        <h2><?= 'Admin '; ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br/>

                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>لوحة تحكم</h3>
                        <ul class="nav side-menu">
                            <li><a href="../adminger/"><img src="../views/img/home.png" width="18" height="18"/> الرئيسية </a>
                            </li>
                            <li><a><img src="../views/img/user.png" width="18" height="18"/> حسابات الأعضاء <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/accounts">إدارة الحسابات</a></li>
                                    <li><a href="../adminger/chang_pwd">تغير كلمة المرور</a></li>
                                  <!--  <li><a href="../adminger/chang_pwd">صلاحيات</a></li>
                                    <li><a href="../adminger/chang_pwd">إضافة مناطق</a></li>
                                    <li><a href="../adminger/chang_pwd">إضافة مدن</a></li>
                                    <li><a href="../adminger/chang_pwd">أنواع الحسابات</a></li> -->
                                    <li><a href="../adminger/chang_pwd">تقارير</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/mgr.png" width="18" height="18"/> حسابات الإدارة <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/management">إدارة الحسابات</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">تغير كلمة المرور</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">صلاحيات</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">تقارير</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/car.png" width="18" height="18"/> السيارات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/cars">إضافة سيارة جديدة</a></li>
                                    <li><a href="../adminger/barnd">إضافة علامة تجارية جديدة - براند</a></li>
                                    <li><a href="../adminger/model"> موديل </a></li>
                                    <!--<li><a href="../adminger/type"> أنواع السيارات </a></li>--> 
                                    <li><a href="../adminger/manifctor"> مصانع السيارات </a></li>                               
                                </ul>
                            </li>
                            
                            <li><a><img src="../views/img/gift.png" width="18" height="18"/> قطع الغيار <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/parts">إضافة قطع غيار</a></li>
                                    <li><a href="../adminger/parts_type">أنواع القطع</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/product.png" width="18" height="18"/>  الخدمات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                <li><a href="../adminger/services_type"> أنواع الخدمات المتاحة</a></li>
                                <li><a href="../adminger/services"> الخدمات</a></li>
                                <li><a href="../adminger/technicians_Specfic"> تخصصات الفنيين  </a></li>
                                <!-- <li><a href="../adminger/technicians"> الفنيين  </a></li>-->
                                   <!-- <li><a href="../adminger/maintenance_workshops"> ورش صيانة</a></li>
                                   <li><a href="../adminger/accessories">مستلزمات السيارة </a></li>
                                    <li><a href="../adminger/compy_padding"> شركات التنجيد </a></li>
                                     -->
                                </ul>
                            </li>


                            <li><a><img src="../views/img/settings.png" width="18" height="18"/> الإعدادات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                <li><a href="../adminger/store"> المتجر </a></li>
                            <li><a href="../adminger/Purchases">  إضافة كمية </a></li> 
                            <li><a href="../adminger/haraj_type">  أنواع الحراج </a></li>
                            <li><a href="../adminger/haraj">  قائمة الحراج </a></li>
                            <li><a href="../adminger/delivery"> التوصيل </a></li>
                            <li><a href="../adminger/area">المنطقة</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="تنظیمات">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="تمام صفحه" onclick="toggleFullScreen();">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="قفل" class="lock_btn">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="خروج" href="../adminger/logout">
                    <img src="../views/img/logout.png" width="18" height="18"/>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>
        <?php 
       
       //echo'<pre>';
       // print_r($products);
       
        ?>
        <!-- top navigation -->
        <div class="top_nav hidden-print">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><img src="../views/img/menu.png" width="18" height="18"/></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o">الإشعارات</i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="../views/img/img.jpg" alt="..." class="img-circle profile_img" style="border-radius:50px;margin: 0px;width:48px;height:48px;margin-left: 10px;"></span>
                                        <span>
                          <span><b>أسم المستخدم</b></span>
                          <span class="time">قبل 4 دقائق</span>
                        </span>
                                        <span class="message">
                          قام بإضافة منتج تابع للمتجر المعني
                        </span>
                                    </a>
                                </li>
                                
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
        <!-- /header content -->
        
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="row top_tiles">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                    <div class="count">179</div>
                    <h3>السيارات</h3>
                    <p>العدد بالمتجر</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-comments-o"></i></div>
                    <div class="count"><span>289</span></div>
                    <h3>القطع</h3>
                    <p>القطع بالمتجر.</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
                    <div class="count">15</div>
                    <h3>الخدمات</h3>
                    <p>الخدمات الموفرة</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-check-square-o"></i></div>
                    <div class="count">2</div>
                    <h3>المستخدمين</h3>
                    <p>القائمين بالنظام</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>تقرير الأنشطة
                            <!--<small>پیشرفت هفتگی</small>-->
                        </h2>
                        <!--<div class="filter">
                            <div id="reportrange" class="pull-left"
                                 style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                <span>اسفند 29, 1394 - فروردین 28, 1395</span> <b class="caret"></b>
                            </div>
                        </div>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <div class="demo-container" style="height:280px">
                                <div id="chart_plot_02" class="demo-placeholder"></div>
                            </div>
                            <div class="tiles">
                                <div class="col-md-4 tile">
                                    <span>عمليات الشراء</span>
                                    <h2>150</h2>
                                    <span class="sparkline11 graph" style="height: 160px;">
                               <canvas width="200" height="60"
                                       style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                          </span>
                                </div>
                                <div class="col-md-4 tile">
                                    <span>الزوار</span>
                                    <h2>231,809</h2>
                                    <span class="sparkline22 graph" style="height: 160px;">
                                <canvas width="200" height="60"
                                        style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                          </span>
                                </div>
                                <div class="col-md-4 tile">
                                    <span>تعداد جلسات</span>
                                    <h2>231,809</h2>
                                    <span class="sparkline11 graph" style="height: 160px;">
                                 <canvas width="200" height="60"
                                         style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                          </span>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div>
                                <div class="x_title">
                                    <h2>الأنشطة</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                               aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">تنظیمات 1</a>
                                                </li>
                                                <li><a href="#">تنظیمات 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>-->
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <ul class="list-unstyled top_profiles scroll-view">
                                    <li class="media event">
                                        <a class="pull-left border-aero profile_thumb">
                                            <img src="../views/img/product.png" />
                                        </a>
                                        <div class="media-body">
                                           
                                        <a class="title" href="#">عنوان المنتج</a>
                                            <p> السعر <strong>200 ريال </strong> </p>
                                            <p>
                                                <small>تاريخ الإدخال</small>
                                            </p>
                                            
                                        </div>
                                    </li>
                                    <li class="media event">
                                        <a class="pull-left border-green profile_thumb">
                                        <img src="../views/img/car.png" />
                                        </a>
                                        <div class="media-body">
                                        <a class="title" href="#">عنوان المنتج</a>
                                            <p> السعر <strong>200 ريال </strong> </p>
                                            <p>
                                                <small>تاريخ الإدخال</small>
                                            </p>        
                                    </div>
                                    </li>
                                    <li class="media event">
                                        <a class="pull-left border-blue profile_thumb">
                                        <img src="../views/img/user.png" />
                                        </a>
                                        <div class="media-body">
                                        <a class="title" href="#">عنوان المنتج</a>
                                            <p> السعر <strong>200 ريال </strong> </p>
                                            <p>
                                                <small>تاريخ الإدخال</small>
                                            </p>
                                        </div>
                                    </li>
                                    <li class="media event">
                                        <a class="pull-left border-aero profile_thumb">
                                        <img src="../views/img/gift.png" />
                                        </a>
                                        <div class="media-body">
                                            <a class="title" href="#">عنوان المنتج</a>
                                            <p> السعر <strong>200 ريال </strong> </p>
                                            <p>
                                                <small>تاريخ الإدخال</small>
                                            </p>
                                        </div>
                                    </li>
                                    <li class="media event">
                                        <a class="pull-left border-green profile_thumb">
                                        <img src="../views/img/user.png" />
                                        </a>
                                        <div class="media-body">
                                            <a class="title" href="#">العضو</a>
                                            <p> الأسم <strong> Admin </strong></p>
                                            <p>
                                                <small>تاريخ التسجيل</small>
                                            </p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>إحصائيات
                            <!--<small>سهم فعالیت</small>-->
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">تنظیمات 1</a>
                                    </li>
                                    <li><a href="#">تنظیمات 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row"
                             style="border-bottom: 1px solid #E0E0E0; padding-bottom: 5px; margin-bottom: 5px;">
                            <div class="col-md-7" style="overflow:hidden;">
                        <span class="sparkline_one" style="height: 160px; padding: 10px 25px;">
                                      <canvas width="200" height="60"
                                              style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                  </span>
                                <h4 style="margin:18px">حركة عمليات الشراء خلال هذا الشهر</h4>
                            </div>

                            <div class="col-md-5">
                                <div class="row" style="text-align: center;">
                                    <div class="col-md-4">
                                        <canvas class="canvasDoughnut" height="110" width="110"
                                                style="margin: 5px auto 10px"></canvas>
                                        <h4 style="margin:0">أكثر 5 براندات طلبا</h4>
                                    </div>
                                    <div class="col-md-4">
                                        <canvas class="canvasDoughnut" height="110" width="110"
                                                style="margin: 5px auto 10px"></canvas>
                                        <h4 style="margin:0">أكثر 5 خدمات طلبا</h4>
                                    </div>
                                    <div class="col-md-4">
                                        <canvas class="canvasDoughnut" height="110" width="110"
                                                style="margin: 5px auto 10px"></canvas>
                                        <h4 style="margin:0">أكثر 5 القطع شرائاً</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-4">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>السيارات
                            <small>حديثة الإضافة</small>
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                        
                            <li><a class="collapse-link"><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"></i></a>
                            </li>
                            <!--<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">تنظیمات 1</a>
                                    </li>
                                    <li><a href="#">تنظیمات 2</a>
                                    </li>
                                </ul>
                            </li>-->
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                    <?php
                      foreach ($products as $val) {
                         echo'<article class="media event">
                         <a class="pull-left date">
                             <p class="month">'.substr(date("F",strtotime($val['p_cdate'])),0,3).'</p>
                             <p class="day">'.explode('-',explode(' ',$val['p_cdate'])[0])[2].'</p>
                         </a>
                         <div class="media-body">
                             <a class="title" href="#">'.$val['p_name'].'</a>
                             <p>'.$val['p_detailes'].'</p>
                         </div>
                     </article>
                    ';  
                      }
                    ?>
                        
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>القطع
                            <small>حديثة الإضافة</small>
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"></a>
                            </li>
                            <!--<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">تنظیمات 1</a>
                                    </li>
                                    <li><a href="#">تنظیمات 2</a>
                                    </li>
                                </ul>
                            </li>-->
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <?php
                      foreach ($parts as $val) {
                         echo'<article class="media event">
                         <a class="pull-left date">
                             <p class="month">'.substr(date("F",strtotime($val['pr_cdate'])),0,3).'</p>
                             <p class="day">'.explode('-',explode(' ',$val['pr_cdate'])[0])[2].'</p>
                         </a>
                         <div class="media-body">
                             <a class="title" href="#">'.$val['pr_name'].'</a>
                             <p>'.$val['pr_detailes'].'</p>
                         </div>
                     </article>
                    ';  
                      }
                    ?>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>الخدمات
                            <small>حديثة الإضافة</small>
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"></a>
                            </li>
                            <!--<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">تنظیمات 1</a>
                                    </li>
                                    <li><a href="#">تنظیمات 2</a>
                                    </li>
                                </ul>
                            </li>-->
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        
                    <?php
                      foreach ($services as $val) {
                         echo'<article class="media event">
                         <a class="pull-left date">
                             <p class="month">'.substr(date("F",strtotime($val['ser_date'])),0,3).'</p>
                             <p class="day">'.explode('-',explode(' ',$val['ser_date'])[0])[2].'</p>
                         </a>
                         <div class="media-body">
                             <a class="title" href="#">'.$val['ser_title'].'</a>
                             <p>'.$val['stype'].'</p>
                         </div>
                     </article>
                    ';  
                      }
                    ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


        <!-- footer content -->
        <footer class="hidden-print" >
            <div class="pull-right">
                جميع الحقوق محفوظة أبو خالد للتجارة
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
<div id="lock_screen">
    <table>
        <tr>
            <td>
                <div class="clock"></div>
                <span class="unlock">
                    <span class="fa-stack fa-5x">
                      <i class="fa fa-square-o fa-stack-2x fa-inverse"></i>
                      <i id="icon_lock" class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                </span>
            </td>
        </tr>
    </table>
</div>
<!-- jQuery -->
<script src="../views/js/jquery-3.5.1.min.js"></script>
<!-- Bootstrap -->
<script src="../views/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../views/js/fastclick.js"></script>
<!-- NProgress -->
<script src="../views/js/nprogress.js"></script>
<!-- bootstrap-progressbar -->
<script src="../views/js/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../views/js/icheck.min.js"></script>

<!-- bootstrap-daterangepicker -->
<script src="../views/js/moment.min.js"></script>

<script src="../views/js/daterangepicker.js"></script>

<!-- Chart.js -->
<script src="../views/js/Chart.min.js"></script>
<!-- jQuery Sparklines -->
<script src="../views/js/jquery.sparkline.min.js"></script>
<!-- gauge.js -->
<script src="../views/js/gauge.min.js"></script>
<!-- Skycons -->
<script src="../views/js/skycons.js"></script>
<!-- Flot -->
<script src="../views/js/jquery.flot.js"></script>
<script src="../views/js/jquery.flot.pie.js"></script>
<script src="../views/js/jquery.flot.time.js"></script>
<script src="../views/js/jquery.flot.stack.js"></script>
<script src="../views/js/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../views/js/jquery.flot.orderBars.js"></script>
<script src="../views/js/jquery.flot.spline.min.js"></script>
<script src="../views/js/curvedLines.js"></script>
<!-- DateJS -->
<script src="../views/js/date.min.js"></script>
<!-- JQVMap -->
<script src="../views/js/jquery.vmap.min.js"></script>
<script src="../views/js/jquery.vmap.world.js"></script>
<script src="../views/js/jquery.vmap.sampledata.js"></script>

<!-- Custom Theme Scripts -->
<script src="../views/js/custom.min.js"></script>

</body>
</html>
