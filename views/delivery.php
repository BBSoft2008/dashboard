<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="fontiran.com:license" content="Y68A9">
    <link rel="icon" href="../views/img/favicon.ico" type="image/ico"/>

    <title>أبو خالد للتجارة</title>

    <!-- Bootstrap -->
    <link href="../views/css/bootstrap.min.css" rel="stylesheet">
    <link href="../views/css/bootstrap-rtl.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../views/css/fontawesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../views/css/nprogress.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="../views/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../views/css/green.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../views/css/daterangepicker.css" rel="stylesheet">
    <link href="../views/css/animate.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../views/css/custom.min.css" rel="stylesheet">
    
<!-- bootstrap-wysiwyg -->
<link href="../views/css/prettify.css" rel="stylesheet">
<!-- Select2 -->
<link href="../views/css/select2.min.css" rel="stylesheet">
<!-- Switchery -->
<link href="../views/css/switchery.min.css" rel="stylesheet">
<link href="../views/css/smart_wizard_all.min.css" rel="stylesheet">
<link href="../views/css/style.css" rel="stylesheet">
<link href='../views/css/jquery.alertable.css' rel="stylesheet">
<!-- starrr -->
<!--<link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">-->
<style>
.btn-info{
    display: none !important;
}
.save_btn,.undo_btn{
    display: none;
}
</style>
   
</head>
<!-- /header content -->
<body class="nav-md">
<div class="container body">
    <div class="main_container">

    
    <div class="col-md-3 left_col hidden-print">
            <div class="left_col scroll-view">
                <!--<div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
                </div>-->

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                    <img src="../views/img/img.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                    <span>مرحبا بك</span>
                        <h2><?= 'Admin '; ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br/>

                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>لوحة تحكم</h3>
                        <ul class="nav side-menu">
                            <li><a href="../adminger/"><img src="../views/img/home.png" width="18" height="18"/> الرئيسية </a>
                            </li>
                            <li><a><img src="../views/img/user.png" width="18" height="18"/> حسابات الأعضاء <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/accounts">إدارة الحسابات</a></li>
                                    <li><a href="../adminger/chang_pwd">تغير كلمة المرور</a></li>
                                  <!--  <li><a href="../adminger/chang_pwd">صلاحيات</a></li>
                                    <li><a href="../adminger/chang_pwd">إضافة مناطق</a></li>
                                    <li><a href="../adminger/chang_pwd">إضافة مدن</a></li>
                                    <li><a href="../adminger/chang_pwd">أنواع الحسابات</a></li> -->
                                    <li><a href="../adminger/chang_pwd">تقارير</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/mgr.png" width="18" height="18"/> حسابات الإدارة <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/management">إدارة الحسابات</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">تغير كلمة المرور</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">صلاحيات</a></li>
                                    <li><a href="../adminger/mgr_chang_pwd">تقارير</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/car.png" width="18" height="18"/> السيارات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/cars">إضافة سيارة جديدة</a></li>
                                    <li><a href="../adminger/barnd">إضافة علامة تجارية جديدة - براند</a></li>
                                    <li><a href="../adminger/model"> موديل </a></li>
                                    <!--<li><a href="../adminger/type"> أنواع السيارات </a></li>-->  
                                    <li><a href="../adminger/manifctor"> مصانع السيارات </a></li>                                 
                                </ul>
                            </li>
                            
                            <li><a><img src="../views/img/gift.png" width="18" height="18"/> قطع الغيار <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../adminger/parts">إضافة قطع غيار</a></li>
                                    <li><a href="../adminger/parts_type">أنواع القطع</a></li>
                                </ul>
                            </li>

                            <li><a><img src="../views/img/product.png" width="18" height="18"/>  الخدمات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                <li><a href="../adminger/services_type"> أنواع الخدمات المتاحة</a></li>
                                <li><a href="../adminger/services"> الخدمات</a></li>
                                <li><a href="../adminger/technicians_Specfic"> تخصصات الفنيين  </a></li>
                                <!-- <li><a href="../adminger/technicians"> الفنيين  </a></li>-->
                                   <!-- <li><a href="../adminger/maintenance_workshops"> ورش صيانة</a></li>
                                   <li><a href="../adminger/accessories">مستلزمات السيارة </a></li>
                                    <li><a href="../adminger/compy_padding"> شركات التنجيد </a></li>
                                     -->
                                </ul>
                            </li>


                            <li><a><img src="../views/img/settings.png" width="18" height="18"/> الإعدادات <span><img style="float:left;" src="../views/img/arrow.png" width="16" height="16"/></span></a>
                                <ul class="nav child_menu">
                                <li><a href="../adminger/store"> المتجر </a></li>
                            <li><a href="../adminger/Purchases">  إضافة كمية </a></li> 
                            <li><a href="../adminger/haraj_type">  أنواع الحراج </a></li>
                            <li><a href="../adminger/haraj">  قائمة الحراج </a></li>
                            <li><a href="../adminger/delivery"> التوصيل </a></li>
                            <li><a href="../adminger/area">المنطقة</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="تنظیمات">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="تمام صفحه" onclick="toggleFullScreen();">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="قفل" class="lock_btn">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="خروج" href="../adminger/logout">
                    <img src="../views/img/logout.png" width="18" height="18"/>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav hidden-print">
        <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><img src="../views/img/menu.png" width="18" height="18"/></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o">الإشعارات</i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="../views/img/img.jpg" alt="..." class="img-circle profile_img" style="border-radius:50px;margin: 0px;width:48px;height:48px;margin-left: 10px;"></span>
                                        <span>
                          <span><b>أسم المستخدم</b></span>
                          <span class="time">قبل 4 دقائق</span>
                        </span>
                                        <span class="message">
                          قام بإضافة منتج تابع للمتجر المعني
                        </span>
                                    </a>
                                </li>
                                
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
        <!-- /header content -->
        
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>إدارة التوصيل</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="جست و جو برای...">
                        <span class="input-group-btn">
                              <button class="btn btn-default" type="button">برو!</button>
                          </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>التوصيل</h2>
                        <!--<ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">تنظیمات 1</a>
                                    </li>
                                    <li><a href="#">تنظیمات 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


<div id="smartwizard">

    <ul class="nav">
        <li class="nav-item">
          <a class="nav-link" href="#step-1">
            إختيار المتجر
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#step-2">
            طرق التوصيل
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#step-3">
            التأكيد
          </a>
        </li>
        <!--<li class="nav-item">
          <a class="nav-link" href="#step-4">
            Step 4
          </a>
        </li>-->
    </ul>

    <div class="tab-content">
        <div id="step-1" class="tab-pane" role="tabpanel" aria-labelledby="step-1">
           

        

        <div class="col-md-4 col-sm-4 col-xs-4" style="margin-bottom:20px;">
                                    <input id="search" type="text" data-validate-length="6,8"
                                           class="form-control col-md-7 col-xs-12" required="required" placeholder="فرز المتاجر بالأسم">
                                </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                            <div id="bubble" style="text-align:center;">
                              <div></div>
                            </div>
                            </div>

                        <table class="table">
                            <thead>
                            
                            <tr>
                                <th>#</th>
                                <th>الحساب</th>
                                <th>تاريخ الإنشاء</th>
                            </tr>
                            </thead>
                            <tbody id="refresh">
                           
                                <?php
                                foreach($stores as $val){
                                    echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['store_id'].'" type="radio"></th>
                                    <td>'.$val['store_title'].'</td>
                                    <td>'.$val['store_cdate'].'</td>'; 
                                }
                                ?>
                                
                            </tbody>
                        </table>

                
        </div>

        <div id="step-2" class="tab-pane" role="tabpane2" aria-labelledby="step-2">
       
       

                        <table style="width:360px;">
                          <tr> <td>
                            <div class="col-md-10 col-sm-10 col-xs-10" style="margin-bottom:20px;">
                                <input id="d-way" type="text" data-validate-length="6,8"
                                class="form-control col-md-7 col-xs-12" required="required" placeholder="طريقة التوصيل">
                                </div>
                           </td>
                           </tr>
                           <tr>
                           <td>
                             <div class="col-md-10 col-sm-10 col-xs-10" style="margin-bottom:20px;">
                               <input id="d-detail" type="text" data-validate-length="6,8"
                                    class="form-control col-md-7 col-xs-12" required="required" placeholder="الوصف">
                                </div>
                           </td>
                           </tr>
                           <tr>
                           <td>
                               <div class="col-md-10 col-sm-10 col-xs-10" style="margin-bottom:20px;">
                                  <input id="d-price" type="number" data-validate-length="6,8"
                                        class="form-control col-md-7 col-xs-12" required="required" placeholder="التكلفة">
                                        </div>             
                            
                           </td>
                           </tr>
                           <tr><td>
                           <div class="col-md-10 col-sm-10 col-xs-10">
                            <div id="bubble2" style="text-align:center;">
                              <div></div>
                            </div>
                            </div>
                           </td></tr>
                        </table>

       
        </div>
        
        <div id="step-3" class="tab-pane" role="tabpane3" aria-labelledby="step-3">
            <p style="margin-top:50px;">هل أنت متأكد من بياناتك المدخله سابقا؟</p>
            <p>ما لم يمكنك العودة الضغط على زر سابقا للتعديل</p>
            <p style="margin-bottom:50px;">أو يمكنك  
             <button id="commit" class="btn btn-success" ><span>إعتماد</span></button>
             مدخلاتك
             <img src="../views/img/ldr.gif" id ="ldr" width="24" height="24"/>
                 <div id="bubble3" style="text-align:center;width:250px;">
                       <div></div>
                    </div>
                   <br/><br/>
            </p>
            <table></table>

        </div>
        
        <!--<div id="step-4" class="tab-pane" role="tabpanel" aria-labelledby="step-4">
            Step 4 Content
        </div>-->
    </div>
</div>


                    </div>
                </div>
            </div>
        </div>


        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>إدارة طرق التوصيل</h2>
                       <!-- <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">تنظیمات 1</a>
                                    </li>
                                    <li><a href="#">تنظیمات 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                    <select name="store_list" id="store_list">
                    <option value="0">أختر معرض</option>
                                   <?php                               
                                      foreach($stores as $val){
                                          echo '<option value="'.$val['store_id'].'">'.$val['store_title'].'</option>';
                                        }
                                   ?>

                    </select>

                    <table class="table">
                            <thead>
                            
                            <tr>
                                <th>#</th>
                                <th>نوع التوصيل</th>
                                <th>السعر</th>
                                <th>الوصف</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="refresh2">
                        
                                
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>





    </div>
</div>
<!-- /page content -->

        <!-- footer content -->
        <footer class="hidden-print">
            <div class="pull-left">
              <!--  Gentelella - قالب پنل مدیریت بوت استرپ <a href="https://colorlib.com">Colorlib</a> | پارسی شده توسط <a
                    href="https://morteza-karimi.ir">مرتضی کریمی</a>-->
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
<div id="lock_screen">
    <table>
        <tr>
            <td>
                <div class="clock"></div>
                <span class="unlock">
                    <span class="fa-stack fa-5x">
                      <i class="fa fa-square-o fa-stack-2x fa-inverse"></i>
                      <i id="icon_lock" class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                </span>
            </td>
        </tr>
    </table>
</div>
<!-- jQuery -->
<script src="../views/js/jquery-3.5.1.min.js"></script>
<!-- Bootstrap -->
<script src="../views/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../views/js/fastclick.js"></script>
<!-- NProgress -->
<script src="../views/js/nprogress.js"></script>
<!-- bootstrap-progressbar -->
<script src="../views/js/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../views/js/icheck.min.js"></script>

<!-- bootstrap-daterangepicker -->
<script src="../views/js/moment.min.js"></script>

<script src="../views/js/daterangepicker.js"></script>

<!-- Chart.js -->
<script src="../views/js/Chart.min.js"></script>
<!-- jQuery Sparklines -->
<script src="../views/js/jquery.sparkline.min.js"></script>
<!-- gauge.js -->
<script src="../views/js/gauge.min.js"></script>
<!-- Skycons -->
<script src="../views/js/skycons.js"></script>
<!-- Flot -->
<script src="../views/js/jquery.flot.js"></script>
<script src="../views/js/jquery.flot.pie.js"></script>
<script src="../views/js/jquery.flot.time.js"></script>
<script src="../views/js/jquery.flot.stack.js"></script>
<script src="../views/js/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../views/js/jquery.flot.orderBars.js"></script>
<script src="../views/js/jquery.flot.spline.min.js"></script>
<script src="../views/js/curvedLines.js"></script>
<!-- DateJS -->
<script src="../views/js/date.min.js"></script>
<!-- JQVMap -->
<script src="../views/js/jquery.vmap.min.js"></script>
<script src="../views/js/jquery.vmap.world.js"></script>
<script src="../views/js/jquery.vmap.sampledata.js"></script>
<script src="../views/js/jquery.alertable.min.js"></script>
<!-- Custom Theme Scripts -->
<script src="../views/js/custom.min.js"></script>
<!-- jQuery Smart Wizard -->
<script src="../views/js/jquery.smartWizard.min.js"></script>
<script type="text/javascript">
        $(document).ready(function(){

            // Toolbar extra buttons
            var btnFinish = $('<button></button>').text('إنتهاء')
                                             .addClass('btn btn-info')
                                             .on('click', function(){ alert('Finish Clicked'); });
            var btnCancel = $('<button></button>').text('إلغاء')
                                             .addClass('btn btn-danger')
                                             .on('click', function(){ $('#smartwizard').smartWizard("reset"); });

            // Step show event
            $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
                $('#bubble div').text('');
                $("#prev-btn").removeClass('disabled');
                $("#next-btn").removeClass('disabled');
                if(stepPosition === 'first') {
                    $("#prev-btn").addClass('disabled');
                } else if(stepPosition === 'last') {
                    $("#next-btn").addClass('disabled');
                } else {
                    $("#prev-btn").removeClass('disabled');
                    $("#next-btn").removeClass('disabled');
                }

                if(stepNumber === 1) {
                    if($('.accID').is(':checked')){
                        $(this).smartWizard("goToStep",1)
                    }else{
                    $(this).smartWizard("goToStep",0)
                    $('#bubble').fadeIn(500).css({'color':'red'}).text('الرجاء إختيار معرض').delay(1000).fadeOut(500,function(){}); 
                   }
                }

                if(stepNumber === 2) {
                    if($('#d-way').val()!="" && $('#d-price').val()!="" && $('#d-detail').val()!=""){
                        $(this).smartWizard("goToStep",2)
                    }else{
                    $(this).smartWizard("goToStep",1)
                    $('#bubble2').fadeIn(500).css({'color':'red'}).text('فضلا قم بملىء كافة الحقول المطلوبة').delay(2000).fadeOut(500,function(){});  
                   }
                }

            });

            // Smart Wizard
            $('#smartwizard').smartWizard({
                selected: 0,
                theme: 'dots', // default, arrows, dots, progress
                // darkMode: true,
                transition: {
                    animation: 'slide-horizontal', // Effect on navigation, none/fade/slide-horizontal/slide-vertical/slide-swing
                },
                toolbarSettings: {
                    toolbarPosition: 'both', // both bottom
                    toolbarExtraButtons: [btnFinish, btnCancel]
                }
            });

            // External Button Events
            $(".btn-danger").on("click", function() {
                // Reset wizard
                $('#d-way,#d-price,#d-detail').val('');
                $('#smartwizard').smartWizard("reset");
                return true;
            });

            $("#prev-btn").on("click", function() {
                // Navigate previous
                $('#smartwizard').smartWizard("prev");
                return true;
            });

            $("#next-btn").on("click", function() {
                // Navigate next
                $('#smartwizard').smartWizard("next");
                return true;
            });


            // Demo Button Events
            $("#got_to_step").on("change", function() {
                // Go to step
                var step_index = $(this).val() - 1;
                $('#smartwizard').smartWizard("goToStep", step_index);
                return true;
            });


            $("#dark_mode").on("click", function() {
                // Change dark mode
                var options = {
                  darkMode: $(this).prop("checked")
                };

                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });

            $("#is_justified").on("click", function() {
                // Change Justify
                var options = {
                  justified: $(this).prop("checked")
                };

                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });

            $("#animation").on("change", function() {
                // Change theme
                var options = {
                  transition: {
                      animation: $(this).val()
                  },
                };
                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });

            $("#theme_selector").on("change", function() {
                // Change theme
                var options = {
                  theme: $(this).val()
                };
                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });



            $('#search').keyup(function(){
    
            $.ajax({
                url:'../adminger/store/filter',
                type:'GET',
                data:{'word':$('#search').val()},
                beforeSend:function(){

                },
                success:function(res){
                    $('#refresh').children().remove();
                    $('#refresh').append(res);
                }
            })
        })


        $('#commit').click(function(){
            //alert($('#d-way').val()+'  '+$('#d-detail').val()+'   '+$('#d-price').val()+'   '+$('.accID:checked').val());
            $.ajax({
                url:'../adminger/delivery/method',
                type:'POST',
                data:{'d-way':$('#d-way').val(),'d-detail':$('#d-detail').val(),'d-price':$('#d-price').val(),'d-store':$('.accID:checked').val()},
                beforeSend:function(){
                    $('#ldr').css({'display':'block'});
                    $('#commit').attr('disabled','disabled');
                },
                success:function(res){
                    //alert(res)
                    $('#ldr').css({'display':'none'});
                    $('#bubble3').fadeIn(1000).css({'color':'green'}).text('تم حفظ البيانات بنجاح').delay(1000).fadeOut(1000,function(){
                            
                            $('#commit').removeAttr('disabled');
                            $('#d-way,#d-detail,#d-price').val('');
                            $('.accID').prop('checked', false);
                            $('#smartwizard').smartWizard("goToStep",0)
                        });
                }
            })
})


            $('#store_list').change(function(){
                
                $.ajax({
                    url:'../adminger/store/delivery',
                    type:'GET',
                    data:{'store_id':$('#store_list option:selected').val()},
                    beforeSend:function(){

                    },
                    success:function(res){
                        $('#refresh2').children().remove();
                        $('#refresh2').append(res);
                    }
                })
            })


            $(document).on('click','.edit_btn',function() {
              
                  var type,price,desc;
                
                  if($(this).parents('tr').find('th .accID').is(':checked')){

                    type=$(this).parents('tr').find('.type').text();
                    prices=$(this).parents('tr').find('.prices').text();
                    desc=$(this).parents('tr').find('.desc').text();
                     
                    $(this).parents('tr').find('.type').attr('contenteditable',true).css({'background-color':'#ededed'});
                    $(this).parents('tr').find('.prices').attr('contenteditable',true).css({'background-color':'#ededed'});
                    $(this).parents('tr').find('.desc').attr('contenteditable',true).css({'background-color':'#ededed'});
                    $(this).css({'display':'none'});
                    $(this).parents('td').append('<img src="../views/img/save.png" width="24" height="24" class="save_btn" style="cursor:pointer;"/>').find('.save_btn').css({'display':'block'});
                    $(this).parents('tr').find('.undo').append('<img src="../views/img/undo.png" width="24" height="24" class="undo_btn" style="cursor:pointer;display:block;"/>');
                  
                }else{
                    $.alertable.alert('الرجاء قوم بإختيار طريقة التوصيل');
                  }
            });

            $(document).on('click','.save_btn',function() {
              
              var type,price,desc;
            
              if($(this).parents('tr').find('th .accID').is(':checked')){

                type=$(this).parents('tr').find('.type').text();
                prices=$(this).parents('tr').find('.prices').text();
                desc=$(this).parents('tr').find('.desc').text();
           
            $.alertable.confirm('هل أنت متأكد?').then(function() {

                $.ajax({
                        url:'../adminger/store/update_delivery',
                        type:'POST',
                        data:{
                            'ss_id':$('.accID:checked').val(),
                            'ss_type':type,
                            'ss_price':prices,
                            'ss_desc':desc,
                            'ss_store':$('#store_list option:selected').val()
                        },
                        beforeSend:function(){

                        },
                        success:function(blk){
                            $.alertable.alert(blk).always(function(){
                                //reload.
                                location.reload();
                                
                            });
                        }
                    });
                });

            }else{
                $.alertable.alert('الرجاء قوم بإختيار طريقة التوصيل');
              }
        });

            $(document).on('click','.undo_btn',function() {

                if($(this).parents('tr').find('th .accID').is(':checked')){

                    type=$(this).parents('tr').find('.type').text();
                    prices=$(this).parents('tr').find('.prices').text();
                    desc=$(this).parents('tr').find('.desc').text();
                    
                    $(this).parents('tr').find('.type').attr('contenteditable',false).css({'background-color':'#ffffff'});
                    $(this).parents('tr').find('.prices').attr('contenteditable',false).css({'background-color':'#ffffff'});
                    $(this).parents('tr').find('.desc').attr('contenteditable',false).css({'background-color':'#ffffff'});
                    $(this).parents('tr').find('.save_btn').remove(); 
                    $(this).parents('tr').find('.edit').append('<img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;display:block;"/>');
                    $(this).parents('tr').find('.undo').children().remove();

                    }


            });


            $(document).on('click','.del_btn',function() {
        var sel='';
        if($('.accID').is(':checked')){
              
            sel=$(this).parents('tr').find('th .accID').val();  

            //$.alertable.alert($(this).parents('tr').find('th .accID').val()); 
            $.alertable.confirm('هل أنت متأكد?').then(function() {
               // $.alertable.alert(sel);

                    $.ajax({
                        url:'../adminger/store/del_delivery',
                        type:'GET',
                        data:{'id':sel},
                        beforeSend:function(){

                        },
                        success:function(blk){
                            $.alertable.alert(blk).always(function(){
                                location.reload();
                            });
                        }
                    });
            });
            
        }else{
            $.alertable.alert('الرجاء إختر حساب مستخدم');
        }
 });


        });
    </script>

</body>
</html>
