<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>أبو خالد للسيارات</title>

    <!-- Bootstrap -->
    <link href="../views/css/bootstrap.min.css" rel="stylesheet">
    <link href="../views/css/bootstrap-rtl.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../views/css/fontawesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../views/css/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../views/css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../views/css/custom.min.css" rel="stylesheet">
    <link href="../views/css/style.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <!--<a class="hiddenanchor" id="signup"></a>-->
      <a class="hiddenanchor" id="signin"></a>
      <a class="hiddenanchor" id="reset"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
          <img src="../views/img/logo.png" />
            <form>
              <h1>تسجيل الدخول</h1>
              <div>
                <input type="text" class="form-control" placeholder="اسم المستخدم" name="u_account" id="u_account" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="كلمة المرور" name="u_pwd" id="u_pwd" required="" />
              </div>
              <div class="btn-mgr">
                <button id="log"><span>دخول</span><img src="../views/img/ldr.gif" id ="ldr" width="24" height="24"/></button>
                <!--<a class="reset_pass" href="#reset">رمز ورود را از دست دادید؟</a>-->
              </div>

              <div class="clearfix"></div>

              <!--<div class="separator">
                <p class="change_link">جدید در سایت؟
                  <a href="#signup" class="to_register"> ایجاد حساب </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                  <p>©1397 تمامی حقوق محفوظ. Gentelella Alela! یک قالب بوت استرپ 3. حریم خصوصی و شرایط</p>
                </div>
              </div>-->
            </form>

            <div id="bubble">
           <div></div>
          </div>

          </section>
        </div>
        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form>
              <h1>ایجاد حساب</h1>
              <div>
                <input type="text" class="form-control" placeholder="نام کاربری" required="" />
              </div>
              <div>
                <input type="email" class="form-control" placeholder="ایمیل" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="رمز ورود" required="" />
              </div>
              <div>
                <a class="btn btn-default submit" href="index.html">ارسال</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">در حال حاضر عضو هستید؟
                  <a href="#signin" class="to_register"> ورود </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                  <p>©<?php echo date('Y-m-d');?> جميع الحقوق محفوظة ل أبوخالد للسيارات</p>
                </div>
              </div>
            </form>
          </section>
        </div>
        <div id="rest_pass" class="animate form rest_pass_form">
          <section class="login_content">
            <!-- /password recovery -->
            <form action="index.html">
              <h1>بازیابی رمز عبور</h1>
              <div class="form-group has-feedback">
                <input type="email" class="form-control" name="email" placeholder="ایمیل" />
                <div class="form-control-feedback">
                  <i class="fa fa-envelope-o text-muted"></i>
                </div>
              </div>
              <button type="submit" class="btn btn-default btn-block">بازیابی رمز عبور </button>
              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">جدید در سایت؟
                  <a href="#signup" class="to_register"> ایجاد حساب </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                  <p>©<?php echo date('Y-m-d');?> جميع الحقوق محفوظة ل أبوخالد للسيارات</p>
                </div>
              </div>
            </form>
            <!-- Password recovery -->
          </section>
          
        </div>
      </div>
    </div>
    <script src="../views/js/jquery-3.5.1.min.js"></script>
    
    <script>
    
    $(document).ready(function(){

      $('form').submit(false);

      $('#log').click(function(){

        $.ajax({
        url:'../adminger/auth',
        type:'POST',
        data:{
            'u_account':$('#u_account').val(),
            'u_pwd':$('#u_pwd').val()
            },
        beforeSend:function(){
          $('#bubble,#ldr').css({'display':'block'});
        },
        success:function(res){
          $('#bubble div').text('');
          if (res==1) {
            $('#ldr').css({'display':'none'});
            $('#log').attr('disabled','disabled');
          
              $('#bubble').fadeIn(1000).css({'color':'green'}).text('تم تسجيل بنجاح').delay(1000).fadeOut(1000,function(){
                window.location.href ='../adminger/dashboard';
              });
             
           
            
            
          }else{
            $('#ldr').css({'display':'none'});
            $('#log').attr('disabled','disabled');
            $('#bubble').fadeIn(1000).css({'color':'#000000'}).text('فضلا تأكد من صحة اسم الحساب / كلمة المرور').delay(1000).fadeOut(1000,function(){
              $('#log').removeAttr('disabled');
            });
          }
          
        }
       });

      });

     

    });

    </script>


  </body>
</html>
