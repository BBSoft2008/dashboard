<?php

class parts_type extends Database{
    
    public function __construct(){}

    public static function getparts_typ(){
      return self::$dbObject->query('select * from parts_type')->fetchall(PDO::FETCH_ASSOC);
    }
      

    public function searchByName($word){
      return (!empty(self::$dbObject))?self::$dbObject->query("select * from parts_type where prt_title LIKE '".$word."%'")->fetchall(PDO::FETCH_ASSOC):null; 
    }

    public function release($id){
      $sql = "Update parts_type set prt_status = 1 Where prt_id =" .$id;
      $res = self::$dbObject->query($sql);
      if($res->rowCount()>0)
         echo 'تم إلغاء الحظر';
      else
         print_r(['message'=>$res->errorCode(),'status'=>false]);
   }
  
   
   public function block($id){
      $sql = "Update parts_type set prt_status = 0 Where prt_id =" .$id;
      $res = self::$dbObject->query($sql);
      if($res->rowCount()>0)
         echo 'تم الحظر';
      else
         print_r(['message'=>$res->errorCode(),'status'=>false]);
   }
  
  
   public function updatePartType($data=[]){
    $sql = 'Update parts_type set prt_title = "'.$data['prt_title']. '",prt_desc = "'.$data['prt_desc'].'" Where prt_id =' . $data['prt_id'];
    //echo $sql;
    $res = self::$dbObject->query($sql);
    if($res->rowCount()>0)
       echo 'تم التعديل بنجاح';
    else
       print_r(['message'=>$res->errorCode(),'status'=>false]);
  }

  public static function getPartTypeByID($id){
    //var_dump(self::$dbObject);
    return self::$dbObject->query("select * from parts_type where prt_id=".$id)->fetch(PDO::FETCH_ASSOC); 
  }

  
}

?>