<?php

class Brand extends Database{
    
    public function __construct(){}


    public static function getBrands(){
        return self::$dbObject->query("select * from brand order by b_sort")->fetchall(PDO::FETCH_ASSOC); 
      }      

      public static function getBrandByID($id){
        //var_dump(self::$dbObject);
        return self::$dbObject->query("select * from brand where b_id=".$id)->fetchall(PDO::FETCH_ASSOC); 
      }  

      public function searchByName($word){
        return self::$dbObject->query("select * from brand where b_title LIKE '".$word."%'")->fetchall(PDO::FETCH_ASSOC); 
      }
      
      public function getBrandType(){
        return self::$dbObject->query('select * from brand_type')->fetchall(PDO::FETCH_ASSOC);
     }

     public function updateBrand($data=[]){
      $sql = "Update brand set b_title = '".$data[1]. "',b_def = '".$data[2]."',b_type = '".$data[3]."' Where b_id =" . $data[0];
      $res = self::$dbObject->query($sql);
      if($res->rowCount()>0)
         echo 'تم التعديل بنجاح';
      else
         print_r(['message'=>$res->errorCode(),'status'=>false]);
   }
}

?>