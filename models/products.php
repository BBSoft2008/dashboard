<?php

class Products extends Database{
    
    public function __construct(){}


    public static function limit($limit){
        //var_dump(self::$dbObject);
        return (!empty(self::$dbObject))?self::$dbObject->query("select p_id, p_name,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, p_mfr, p_detailes, p_comment, p_cdate, currency.cu_comment as currency, p_price, p_img, is_available, is_haraj, members.m_fname fname, members.m_lname as lname,members.m_img as avatar, p_tag, p_mileage,models.mo_title as model, trans.tr_trype as trans, fuel_type.f_type as fuel, p_year from products INNER JOIN members ON products.p_createdBy = members.m_id INNER JOIN currency ON products.p_currency = currency.cu_id INNER JOIN fuel_type ON products.p_fuel = fuel_type.f_id INNER JOIN trans ON products.p_trans = trans.tr_id JOIN models on products.p_model=models.mo_id JOIN brand on products.p_brand = brand.b_id ORDER BY p_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC):null; 
      }
     
      
      public static function isHaraj($limit){
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select p_id, p_name,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, p_mfr, p_detailes, p_comment, p_cdate, currency.cu_comment as currency, p_price, p_img, is_available, is_haraj,members.m_fname fname, members.m_lname as lname,members.m_img as avatar, p_tag, p_mileage,models.mo_title as model, trans.tr_trype as trans, fuel_type.f_type as fuel, p_year from products INNER JOIN members ON products.p_createdBy = members.m_id INNER JOIN currency ON products.p_currency = currency.cu_id INNER JOIN fuel_type ON products.p_fuel = fuel_type.f_id INNER JOIN trans ON products.p_trans = trans.tr_id JOIN models on products.p_model=models.mo_id JOIN brand on products.p_brand = brand.b_id where is_haraj= 1 ORDER BY p_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null);
      }

      public static function isStore($limit){
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select p_id, p_name,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, p_mfr, p_detailes, p_comment, p_cdate, currency.cu_comment as currency, p_price, p_img, is_available, is_haraj,members.m_fname fname, members.m_lname as lname,members.m_img as avatar, p_tag, p_mileage,models.mo_title as model, trans.tr_trype as trans, fuel_type.f_type as fuel, p_year from products INNER JOIN members ON products.p_createdBy = members.m_id INNER JOIN currency ON products.p_currency = currency.cu_id INNER JOIN fuel_type ON products.p_fuel = fuel_type.f_id INNER JOIN trans ON products.p_trans = trans.tr_id JOIN models on products.p_model=models.mo_id JOIN brand on products.p_brand = brand.b_id where is_haraj= 0 ORDER BY p_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null);
      }


      public function searchByName($word){
        return (!empty(self::$dbObject))?self::$dbObject->query("select p_id, p_name,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, p_mfr, p_detailes, p_comment, p_cdate, currency.cu_comment as currency, p_price, p_img, is_available, is_haraj, members.m_fname fname, members.m_lname as lname,members.m_img as avatar, p_tag, p_mileage,models.mo_title as model, trans.tr_trype as trans, fuel_type.f_type as fuel, p_year from products INNER JOIN members ON products.p_createdBy = members.m_id INNER JOIN currency ON products.p_currency = currency.cu_id INNER JOIN fuel_type ON products.p_fuel = fuel_type.f_id INNER JOIN trans ON products.p_trans = trans.tr_id JOIN models on products.p_model=models.mo_id JOIN brand on products.p_brand = brand.b_id where p_name LIKE '".$word."%'")->fetchall(PDO::FETCH_ASSOC):null; 
      }

      public function getProductById($id){
        return self::$dbObject->query('select * from products where p_id='.$id)->fetchall(PDO::FETCH_ASSOC);
     }

     public function getProducts(){
      return self::$dbObject->query('select * from products ')->fetchall(PDO::FETCH_ASSOC);
   }

     public function updateProduct($data=[]){
      $sql = "Update products set p_name = '".$data['p_name']. "',p_model = '".$data['p_model']."',vin_no = '".$data['vin_no'].
      "',is_new = '".$data['is_new']."',p_mfr = '".$data['p_mfr']."',p_detailes = '".$data['p_detailes']."',p_comment = '".
      $data['p_comment']."',p_brand = '".$data['p_brand']."',p_currency = '".$data['p_currency']."',p_price = '".$data['p_price'].
      "',is_available = '".$data['is_available']."',p_fuel = '".$data['p_fuel']."',p_store = '".$data['p_store'].
      "',is_orginal = '".$data['orginal']."' Where p_id =" . $data['p_id'];

      //echo $sql;
      $res = self::$dbObject->query($sql);

      if($res->rowCount()>0)
         echo 'تم التعديل بنجاح' ;
      else
         print_r(['message'=>$sql  ,'status'=>false]);
   }

   public function block($id){
    $sql = "Update products set is_available = 0 Where p_id =" .$id;
    $res = self::$dbObject->query($sql);
    if($res->rowCount()>0)
       echo 'أصبح المنتج غير متاح';
    else
       print_r(['message'=>$sql,'status'=>false]);
 }

 public function release($id){
  $sql = "Update products set is_available = 1 Where p_id =" .$id;
  $res = self::$dbObject->query($sql);
  if($res->rowCount()>0)
     echo 'أصبح المنتج متاح';
  else
     print_r(['message'=>$sql,'status'=>false]);
}

}

?>