<?php

class Store extends Database{
    
    public function __construct(){}


    public static function limit($limit){
        //var_dump(self::$dbObject);
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select p_id, p_name,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, p_mfr, p_detailes, p_comment, p_cdate, currency.cu_currency as currency, p_price, p_img, is_available, is_haraj, members.m_account as name, p_tag, p_mileage,models.mo_title as model, trans.tr_trype as trans, fuel_type.f_type as fuel, p_year from products INNER JOIN members ON products.p_createdBy = members.m_id INNER JOIN currency ON products.p_currency = currency.cu_id INNER JOIN fuel_type ON products.p_fuel = fuel_type.f_id INNER JOIN trans ON products.p_trans = trans.tr_id JOIN models on products.p_model=models.mo_id JOIN brand on products.p_brand = brand.b_id ORDER BY p_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
      }

      public static function Get_store(){
        //var_dump(self::$dbObject);
       // print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select * from store ")->fetchall(PDO::FETCH_ASSOC)):null); 
        return self::$dbObject->query('select * from store')->fetchall(PDO::FETCH_ASSOC);
      }
      
      public static function getStoreBy($id){
        //var_dump(self::$dbObject);
       // print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select * from store ")->fetchall(PDO::FETCH_ASSOC)):null); 
        return self::$dbObject->query('select * from store where store_owner='.$id.' order by store_id desc')->fetchall(PDO::FETCH_ASSOC);
      }

      public static function searchByName($word){
        return self::$dbObject->query("SELECT store_id,store_title,store_cdate,store_status FROM store where store_title LIKE '".$word."%'")->fetchall(PDO::FETCH_ASSOC);
     }

     public static function searchStoreByName($word){
      return self::$dbObject->query("SELECT store_id,store_title,store_cdate FROM store where store_title LIKE '".$word."%'")->fetchall(PDO::FETCH_ASSOC);
   }

   public static function getStores(){
    return self::$dbObject->query('select * from store order by store_id desc')->fetchall(PDO::FETCH_ASSOC);
   }

   public function getStoreById($id){
    return self::$dbObject->query('select *,area.area_id as area_id,area.area_name as area_name,area.country as country from store inner join area on store.store_area = area.area_id where store_id='.$id)->fetchall(PDO::FETCH_ASSOC);
   }

   public static function getStoresDeliveryMethode($store_id){
    return self::$dbObject->query('select * from store_shipping where ss_store='.$store_id)->fetchall(PDO::FETCH_ASSOC);
   }

   public static function delete($ss_id){
    return self::$dbObject->exec('delete from store_shipping where ss_id='.$ss_id);
   }


   public function release($id){
    $sql = "Update store set store_status = 1 Where store_id =" .$id;
    $res = self::$dbObject->query($sql);
    if($res->rowCount()>0)
       echo 'تم إلغاء الحظر';
    else
       print_r(['message'=>$res->errorCode(),'status'=>false]);
 }

 
 public function block($id){
    $sql = "Update store set store_status = 0 Where store_id =" .$id;
    $res = self::$dbObject->query($sql);
    if($res->rowCount()>0)
       echo 'تم الحظر';
    else
       print_r(['message'=>$res->errorCode(),'status'=>false]);
 }


 public function updateStore($data=[]){
  $sql = 'Update store set store_title = "'.$data[1]. '",store_address = "'.$data[2].'",store_latlang = "'.$data[3].
  '",store_area = "'.$data[4].'",store_detial = "'.$data[5].'" Where store_id =' . $data[0];
 // echo $sql;
  $res = self::$dbObject->query($sql);
  if($res->rowCount()>0)
     echo 'تم التعديل بنجاح';
  else
     print_r(['message'=>$res->errorCode(),'status'=>false]);
}

}

?>