<?php

class Members extends Database{
    
    public function __construct(){}

    public function login($data=[]){
        $res = self::$dbObject->query('select m_id, m_fname, m_lname, m_gender, m_country, m_area, m_status, m_account, m_pwd, m_mobile, m_email, country.c_name, area.area_name, m_address, m_utype, m_cdate, m_comment, m_img FROM members INNER join country ON members.m_country = country.c_id INNER JOIN area ON members.m_area = area.area_id WHERE m_status=2 and members.m_mobile='.$data[0])->fetch(PDO::FETCH_ASSOC);

        if(password_verify($data[1],$res['m_pwd'])){
           unset($res['m_pwd']);
           return $res;
        }else
           return false;
      }

      public function getTypes(){
         return self::$dbObject->query('select * from users_type')->fetchall(PDO::FETCH_ASSOC);
      }

      public function getCountery(){
         return self::$dbObject->query('select * from country')->fetchall(PDO::FETCH_ASSOC);
      }

      public function getusr_store_owner(){
         return self::$dbObject->query('select * from members where m_utype <> 4 AND m_utype <> 5')->fetchall(PDO::FETCH_ASSOC);
      }

      public function get_curreny(){
         return self::$dbObject->query('SELECT * FROM currency')->fetchall(PDO::FETCH_ASSOC);
      }

      public function accounts(){
         return self::$dbObject->query("SELECT m_id,m_account,m_cdate,m_status FROM members order by m_id desc limit 20")->fetchall(PDO::FETCH_ASSOC);
      }

      public function getMemberById($id){
         return self::$dbObject->query('select * from members where m_id='.$id)->fetchall(PDO::FETCH_ASSOC);
      }

      public function searchByName($word){
         return self::$dbObject->query("SELECT m_id,m_account,m_cdate FROM members where m_account LIKE '".$word."%'")->fetchall(PDO::FETCH_ASSOC);
      }

      public function searchByNameV2($word){
         return self::$dbObject->query("SELECT m_id,m_account,m_cdate FROM members where m_account LIKE '".$word."%' order by m_id desc")->fetchall(PDO::FETCH_ASSOC);
      }


      public function updateMember($data=[]){
         $sql = "Update members set m_fname = '".$data['fname']. "',m_lname = '".$data['lname']."',m_gender = '".$data['gender']."',m_account = '".$data['u_account']."',m_mobile = '".$data['mobile']."',m_email = '".$data['email']."',m_country = '".$data['country']."',m_address = '".$data['maddress']."',m_utype = '".$data['m_utype']."',m_comment = '".$data['u_comment']."' Where m_id =" . $data['m_id'];
         $res = self::$dbObject->query($sql);
         if($res->rowCount()>0)
            echo 'تم التعديل بنجاح';
         else
            print_r(['message'=>$res->errorCode(),'status'=>false]);
      }

      public function release($id){
         $sql = "Update members set m_status = 1 Where m_id =" .$id;
         $res = self::$dbObject->query($sql);
         if($res->rowCount()>0)
            echo 'تم إلغاء الحظر';
         else
            print_r(['message'=>$res->errorCode(),'status'=>false]);
      }

      
      public function block($id){
         $sql = "Update members set m_status = 3 Where m_id =" .$id;
         $res = self::$dbObject->query($sql);
         if($res->rowCount()>0)
            echo 'تم الحظر';
         else
            print_r(['message'=>$res->errorCode(),'status'=>false]);
      }

      
      public static function update_pwd($id,$new_pwd,$old_pwd){
         $sql = "Select m_pwd from members where m_id = " . $id; 
         $res = self::$dbObject->query($sql);
         
         if($res->rowCount()>0)
         {
          // if(password_verify($old_pwd,$res->fetch(PDO::FETCH_ASSOC)['m_pwd'])){
              $sql = "Update members set m_pwd = '".password_hash($new_pwd ,PASSWORD_BCRYPT)."' Where m_id =" . $id;
              $res = self::$dbObject->query($sql);
              if($res->rowCount()>0){
                 //print_r(json_encode(['message'=>'Modified successfully.' .$sql,'status'=>true]));
                 return true;
              }else{
                  //print_r(json_encode(['message'=>$res->errorCode(),'status'=>false]));
                 return false;
              }
                
           }
          /* else 
            print_r(json_encode(['message'=>"old pwd not true",'status'=>false]));
             }*/
           else
              return false;
      }

      
}

?>