<?php

class Parts extends Database{
    
    public function __construct(){}


    public static function limit($limit){
        //var_dump(self::$dbObject);
        return (!empty(self::$dbObject))?self::$dbObject->query("select pr_id, pr_name,parts_type.prt_title as part,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, pr_mfr, pr_detailes, pr_comment, pr_cdate, currency.cu_currency as currency, pr_price, pr_img, is_available, is_haraj, members.m_fname fname, members.m_lname as lname, members.m_img as avatar, pr_tag, models.mo_title as model, pr_year from parts INNER JOIN members ON parts.pr_createdBy = members.m_id INNER JOIN currency ON parts.pr_currency = currency.cu_id JOIN models on parts.pr_model=models.mo_id JOIN brand on parts.pr_brand = brand.b_id INNER JOIN parts_type ON parts.pr_type=parts_type.prt_id ORDER BY pr_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC):null; 
      }
      
      public static function isHaraj($limit){
        //var_dump(self::$dbObject);
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select pr_id, pr_name,parts_type.prt_title as part,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, pr_mfr, pr_detailes, pr_comment, pr_cdate, currency.cu_currency as currency, pr_price, pr_img, is_available, is_haraj, members.m_fname fname, members.m_lname as lname,members.m_img as avatar, pr_tag, models.mo_title as model, pr_year from parts INNER JOIN members ON parts.pr_createdBy = members.m_id INNER JOIN currency ON parts.pr_currency = currency.cu_id JOIN models on parts.pr_model=models.mo_id JOIN brand on parts.pr_brand = brand.b_id INNER JOIN parts_type ON parts.pr_type=parts_type.prt_id where is_haraj=1 ORDER BY pr_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
      }

      public static function isStore($limit){
        //var_dump(self::$dbObject);
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select pr_id, pr_name,parts_type.prt_title as part,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, pr_mfr, pr_detailes, pr_comment, pr_cdate, currency.cu_currency as currency, pr_price, pr_img, is_available, is_haraj, members.m_fname fname, members.m_lname as lname, members.m_img as avatar, pr_tag, models.mo_title as model, pr_year from parts INNER JOIN members ON parts.pr_createdBy = members.m_id INNER JOIN currency ON parts.pr_currency = currency.cu_id JOIN models on parts.pr_model=models.mo_id JOIN brand on parts.pr_brand = brand.b_id INNER JOIN parts_type ON parts.pr_type=parts_type.prt_id where is_haraj=0 ORDER BY pr_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
      }
      public function searchByName($word){
        return (!empty(self::$dbObject))?self::$dbObject->query("select pr_id, pr_name,parts_type.prt_title as part,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, pr_mfr, pr_detailes, pr_comment, pr_cdate, currency.cu_currency as currency, pr_price, pr_img, is_available, is_haraj, members.m_fname fname, members.m_lname as lname, members.m_img as avatar, pr_tag, models.mo_title as model, pr_year from parts INNER JOIN members ON parts.pr_createdBy = members.m_id INNER JOIN currency ON parts.pr_currency = currency.cu_id JOIN models on parts.pr_model=models.mo_id JOIN brand on parts.pr_brand = brand.b_id INNER JOIN parts_type ON parts.pr_type=parts_type.prt_id where pr_name LIKE '".$word."%'")->fetchall(PDO::FETCH_ASSOC):null; 
      }

      public function getPartstById($id){
        return self::$dbObject->query('select * from parts where pr_id='.$id)->fetchall(PDO::FETCH_ASSOC);
     }

     public function updateParts($data=[]){
      $sql = "Update parts set pr_name = '".$data['pr_name']. "',pr_model = '".$data['pr_model']."',vin_no = '".$data['vin_no'].
      "',is_new = '".$data['is_new']."',pr_mfr = '".$data['pr_mfr']."',pr_detailes = '".$data['pr_detailes']."',pr_comment = '".
      $data['pr_comment']."',pr_brand = '".$data['pr_brand']."',pr_currency = '".$data['pr_currency']."',pr_price = '".$data['pr_price'].
      "',is_available = '".$data['is_available']."',pr_type = '".$data['pr_type']."',pr_store = '".$data['pr_store'].
      "',is_orginal = '".$data['orginal']."' Where pr_id =" . $data['pr_id'];
      //echo $sql ;
      
      $res = self::$dbObject->query($sql);

      if($res->rowCount()>0)
         echo 'تم التعديل بنجاح' ;
      else
         print_r(['message'=>$sql  ,'status'=>false]);
   }

   public function block($id){
    $sql = "Update parts set is_available = 0 Where pr_id =" .$id;
    $res = self::$dbObject->query($sql);
    if($res->rowCount()>0)
       echo 'أصبح المنتج غير متاح';
    else
       print_r(['message'=>$sql,'status'=>false]);
 }

 public function release($id){
  $sql = "Update parts set is_available = 1 Where pr_id =" .$id;
  $res = self::$dbObject->query($sql);
  if($res->rowCount()>0)
     echo 'أصبح المنتج متاح';
  else
     print_r(['message'=>$sql,'status'=>false]);
}

}

?>