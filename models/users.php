<?php

class Users extends Database{
    
    public function __construct(){}

    public function accounts(){
        return self::$dbObject->query("SELECT u_id,u_account,u_cdate,u_status FROM users order by u_id desc limit 20")->fetchall(PDO::FETCH_ASSOC);
     }

     public function allAccounts(){
      return self::$dbObject->query("SELECT u_id,u_account FROM users order by u_id desc")->fetchall(PDO::FETCH_ASSOC);
   }

     public function searchByName($word){
        return self::$dbObject->query("SELECT u_id,u_account,u_cdate FROM users where u_account LIKE '".$word."%' order by u_id desc")->fetchall(PDO::FETCH_ASSOC);
     }
     
     public function searchByMGRName($word){
      return self::$dbObject->query("SELECT u_id,u_account,u_cdate FROM users where u_account LIKE '".$word."%' order by u_id desc")->fetchall(PDO::FETCH_ASSOC);
   }

     public function getTypes(){
      return self::$dbObject->query('select * from users_type')->fetchall(PDO::FETCH_ASSOC);
   }

   public function getCountery(){
      return self::$dbObject->query('select * from country')->fetchall(PDO::FETCH_ASSOC);
   }
   
   public function getMGRById($id){
      return self::$dbObject->query('select * from users where u_id='.$id)->fetchall(PDO::FETCH_ASSOC);
   }

   public function updateMGR($data=[]){
      $sql = "Update users set u_fname = '".$data['fname']. "',u_lname = '".$data['lname']."',u_gender = '".$data['gender']."',u_account = '".$data['u_account']."',u_mobile = '".$data['mobile']."',u_email = '".$data['email']."',u_country = '".$data['country']."',u_address = '".$data['maddress']."',u_utype = '".$data['u_utype']."',u_comment = '".$data['u_comment']."' Where u_id =" . $data['u_id'];
      $res = self::$dbObject->query($sql);
      if($res->rowCount()>0)
         echo 'تم التعديل بنجاح';
      else
         print_r(['message'=>$res->errorCode(),'status'=>false]);
   }

   public function release($id){
      $sql = "Update users set u_status = 1 Where u_id =" .$id;
      $res = self::$dbObject->query($sql);
      if($res->rowCount()>0)
         echo 'تم إلغاء الحظر';
      else
         print_r(['message'=>$res->errorCode(),'status'=>false]);
   }

   
   public function block($id){
      $sql = "Update users set u_status = 3 Where u_id =" .$id;
      $res = self::$dbObject->query($sql);
      if($res->rowCount()>0)
         echo 'تم الحظر';
      else
         print_r(['message'=>$res->errorCode(),'status'=>false]);
   }


   public static function update_pwd($id,$new_pwd,$old_pwd){
      $sql = "Select u_pwd from users where u_id = " . $id; 
      $res = self::$dbObject->query($sql);
      
      if($res->rowCount()>0)
      {
      //  if(password_verify($old_pwd,$res->fetch(PDO::FETCH_ASSOC)['u_pwd'])){
           $sql = "Update users set u_pwd = '".password_hash($new_pwd ,PASSWORD_BCRYPT)."' Where u_id =" . $id;
           $res = self::$dbObject->query($sql);
           if($res->rowCount()>0){
              //print_r(json_encode(['message'=>'Modified successfully.' .$sql,'status'=>true]));
              return true;
           }else{
               //print_r(json_encode(['message'=>$res->errorCode(),'status'=>false]));
              return false;
           }
             
       /* }
        else 
         print_r(json_encode(['message'=>"old pwd not true",'status'=>false]));*/
          }
        else
           return false;
   }



}

?>