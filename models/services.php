<?php

class services extends Database{
    
    public function __construct(){}


      public function getservice(){
         return self::$dbObject->query('select * from services_type')->fetchall(PDO::FETCH_ASSOC);
      } 
      
      public function limit($id){
         return self::$dbObject->query('select *,services_type.sert_type as stype from services INNER JOIN services_type on services.ser_type=services_type.sert_id order by ser_id limit '.$id)->fetchall(PDO::FETCH_ASSOC);
      } 
      public function getsallervice(){
         return self::$dbObject->query('select ser_id,ser_type,ser_periods,ser_address,ser_latlang,ser_owner,ser_details,ser_area,ser_date,ser_country,ser_thumb,ser_title,store.store_title as owner,services_type.sert_type as stype,sert_status from services INNER JOIN store ON services.ser_owner = store.store_id INNER JOIN services_type ON services.ser_type = services_type.sert_id')->fetchall(PDO::FETCH_ASSOC);
      } 
      
      
      public function getServicesByType($t){
         return self::$dbObject->query('select ser_id,ser_type,ser_periods,ser_address,ser_latlang,ser_owner,ser_details,ser_area,ser_date,ser_country,ser_thumb,ser_title,store.store_title as owner,services_type.sert_type as stype,sert_status from services INNER JOIN store ON services.ser_owner = store.store_id INNER JOIN services_type ON services.ser_type = services_type.sert_id where ser_type='.$t)->fetchall(PDO::FETCH_ASSOC);
      } 

      public function getServiceById($id){
         return self::$dbObject->query('select ser_id,ser_type,ser_periods,ser_address,ser_latlang,ser_owner,ser_details,ser_area,ser_date,ser_country,ser_thumb,ser_title,store.store_title as owner,services_type.sert_type as stype,sert_status from services INNER JOIN store ON services.ser_owner = store.store_id INNER JOIN services_type ON services.ser_type = services_type.sert_id where ser_id='.$id)->fetch(PDO::FETCH_ASSOC);
      } 

      public function searchByName($word){
         return self::$dbObject->query("select * from services where b_title LIKE '".$word."%'")->fetchall(PDO::FETCH_ASSOC); 
       }

       public function updateService($data=[]){
         $sql = "Update services set ser_type = '".$data[1]. "',ser_periods = '".$data[2]."',ser_address = '".$data[3]."',ser_latlang = '".$data[4]."',ser_owner = '".$data[5]."',ser_details = '".$data[6]."',ser_area = '".$data[7]."',ser_country = '".$data[8]."',ser_title = '".$data[9]."' Where ser_id =" . $data[0];
         $res = self::$dbObject->query($sql);
         if($res->rowCount()>0)
            echo 'تم التعديل بنجاح';
         else
            print_r(['message'=>$res->errorCode(),'status'=>false]);
      }


      public function release($id){
         $sql = "Update services set sert_status = 1 Where ser_id =" .$id;
         $res = self::$dbObject->query($sql);
         if($res->rowCount()>0)
            echo 'تم إلغاء الحظر';
         else
            print_r(['message'=>$res->errorCode(),'status'=>false]);
      }

      
      public function block($id){
         $sql = "Update services set sert_status = 0 Where ser_id =" .$id;
         $res = self::$dbObject->query($sql);
         if($res->rowCount()>0)
            echo 'تم الحظر';
         else
            print_r(['message'=>$res->errorCode(),'status'=>false]);
      }

}

?>