<?php

class haraj extends Database{
    
    public function __construct(){}


      public function getHaraj(){
        return self::$dbObject->query('select post_id,post_title,post_price,status.s_type as status,members.m_account as nick_name, post_createdBy ,post_cdate,currency.cu_currency as currency,post_view_count from posts inner join post_type on posts.p_type=post_type.pt_id inner join status on posts.post_status = status.s_id inner join members on posts.post_createdBy=members.m_id inner join currency on posts.post_cur = currency.cu_id order by post_id limit 20')->fetchall(PDO::FETCH_ASSOC);
     }

     public function getHarajByFilter($post_type,$post_cat,$status){
      return self::$dbObject->query('select post_id,post_title,post_price,status.s_type as status,members.m_account as nick_name, post_createdBy ,post_cdate,currency.cu_currency as currency,post_view_count from posts inner join post_type on posts.p_type=post_type.pt_id inner join status on posts.post_status = status.s_id inner join members on posts.post_createdBy=members.m_id inner join currency on posts.post_cur = currency.cu_id where post_cat='.$post_cat.' and p_type='.$post_type.' and post_status='.$status.' order by post_id limit 20')->fetchall(PDO::FETCH_ASSOC);
   }
    
   public function getPostType(){
      return self::$dbObject->query('select * from post_type')->fetchall(PDO::FETCH_ASSOC);
   }

   public function getStatus(){
      return self::$dbObject->query('select * from status')->fetchall(PDO::FETCH_ASSOC);
   }

   public function category(){
      return self::$dbObject->query('select * from category')->fetchall(PDO::FETCH_ASSOC);
   }
   
}


?>