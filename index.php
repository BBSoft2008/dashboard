<?php
session_start();

//header('Content-Type: application/json');
header('Content-Type: text/html; charset=utf-8');
// working on it.
require 'flight/Flight.php';
require_once 'auto_load.php';

/*use paragraph1\phpFCM\Client;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\Device;
use paragraph1\phpFCM\Notification;

require_once 'vendor/autoload.php';
*/

Database::engage(new MySQL());

$m = new Members();
$d = new Store_shipping();
$u = new Users();
$p = new products();
$br = new brand(); 
$pr = new parts();
$mo = new models();
$s = new store();
$haraj = new haraj();
$manuf = new manufacturer();
$pr_typ = new parts_type();
$p_typ = new product_type();
$fl_typ = new fuel_type();
$portfo = new portfolio();
$area = new Area();
$serv = new services();
$serv_type = new services_type();
$specialization = new specialization();
$mk = new mkzn();
$ked = new ked_money();

Flight::set('flight.views.path', './views/');


Flight::route('/adminger', function(){
  if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else
     Flight::redirect('adminger/dashboard');

});




Flight::route('/adminger/logout', function(){
         session_destroy();
       Flight::redirect('adminger/signin');
  
  });
  

Flight::route('/adminger/dashboard', function()use($p,$pr,$serv){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
        
     Flight::render('dashboard.php',[
      'products'=>$p->limit(5),
      'parts'=>$pr->limit(5),
      'services'=>$serv->limit(5)
     ]);
    }
});

Flight::route('/adminger/store', function()use($m,$s,$u){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else

    Flight::render('add_store.php',array(
        'types'=>$m->getTypes(),
        'countery'=>$m->getCountery(),
        'owners' => $u->allAccounts(),
        'store'=>$s->Get_store()
    ));
});

Flight::route('/adminger/area',function()use($m) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else
        Flight::render('area.php',array(
            'countery'=>$m->getCountery()));
});

Flight::route('/adminger/delivery',function()use($s,$d) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
        Flight::render('delivery.php',array(
            'store'=>$s->getStoreBy($_SESSION['uid']),
            'stores'=>$s->getStores()
        ));
    }
});


Flight::route('/adminger/store/delivery',function()use($s,$d) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
       
        $word = Flight::request()->query->store_id;

        //print_r($m->searchByName($word));
   
          foreach($s->getStoresDeliveryMethode($word) as $val){
           echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['ss_id'].'" type="radio"></th>
               <td class="type">'.$val['ss_type'].'</td>
               <td class="prices">'.$val['ss_price'].'</td>
               <td class="desc">'.$val['ss_desc'].'</td>
               <td class="edit" style="width:32px;height:32px;">
               <img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/>
               </td>
               <td class="undo" style="width:32px;height:32px;"></td>
               <td style="width:32px;height:32px;"><img src="../views/img/del.png" width="24" height="24" class="del_btn" style="cursor:pointer;"/></td>';
       
       }

    }
});


Flight::route('GET /adminger/store/del_delivery',function()use($s) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    //print_r($data);
    if($s->delete($id)>0)
      echo 'تم الحذف بنجاح';
      else
       echo 'هنالك مشكلة';
    }
});




Flight::route('/adminger/Purchases', function() use($m,$p){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else
    //$p = $mo->getBarnd(); 
     Flight::render('add_qty.php',array(
        'product'=>$p->getProducts(),'curr'=>$m->get_curreny()));
});

Flight::route('POST /adminger/delivery/method', function()use($d){
    
    $data = Flight::request()->data;
    //print_r($data);
    $d->insert([
        '',
        $data['d-way'],
        $data['d-price'],
        $data['d-detail'],
        $data['d-store']
    ]);

});



Flight::route('GET /adminger/accounts/release',function()use($m) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    //print_r($data);
    $m->release($id);

    }
});

Flight::route('GET /adminger/services/release',function()use($serv) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    //print_r($data);
    $serv->release($id);
    }
});

Flight::route('GET /adminger/store/release',function()use($s) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    //print_r($data);
    $s->release($id);

    }
});

Flight::route('GET /adminger/product/release',function()use($pr) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    //print_r($data);
    $pr->release($id);
    }
});

Flight::route('GET /adminger/part_type/release',function()use($pr_typ) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    //echo $id;
    $pr_typ->release($id);
    }
});

Flight::route('GET /adminger/part_type/block',function()use($pr_typ) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    //echo $id;
    $pr_typ->block($id);
    }
});

Flight::route('/adminger/product',function()use($p) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else
    $id = Flight::request()->query->id;
    print_r(json_encode($p->getProductById($id)));
});

Flight::route('/adminger/specialization/data',function()use($specialization) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else
    $id = Flight::request()->query->id;
    print_r(json_encode($specialization->getSpecializationById($id)));
});



Flight::route('/adminger/parts/modify',function()use($pr) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else
    $id = Flight::request()->query->id;
    print_r(json_encode($pr->getPartstById($id)));
});

Flight::route('GET /adminger/accounts/block',function()use($m) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    //print_r($data);
    $m->block($id);
    }
});

Flight::route('GET /adminger/mgr/block',function()use($u) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    //print_r($data);
    $u->block($id);
    }
});

Flight::route('GET /adminger/mgr/release',function()use($u) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    //print_r($data);
    $u->release($id);
    }
});

Flight::route('GET /adminger/services/block',function()use($serv) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    //print_r($data);
    $serv->block($id);
    }
});


Flight::route('GET /adminger/store/block',function()use($s) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    //print_r($data);
    $s->block($id);
    }
});

Flight::route('GET /adminger/product/block',function()use($p) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    $p->block($id);
    }
});

Flight::route('GET /adminger/parts/block',function()use($pr) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    $pr->block($id);
    }
});


Flight::route('POST /adminger/members/update',function()use($m) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $data = Flight::request()->data;
    //print_r($data);
    $m->updateMember($data);
    }
});

Flight::route('POST /adminger/specialization/update',function()use($specialization) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $data = Flight::request()->data;
    //print_r($data);
    $specialization->updateSpecialization($data);
    }
});

Flight::route('POST /adminger/store/update_delivery',function()use($d) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $data = Flight::request()->data;
    //print_r($data);
    //echo $data['ss_store'];
    $d->updateDelivery($data);
    }
});

Flight::route('POST /adminger/product/update',function()use($p) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $data = Flight::request()->data;
    //print_r($data);
    $p->updateProduct($data);
    }
});


Flight::route('POST /adminger/part_type/update',function()use($pr_typ) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $data = Flight::request()->data;
    //print_r($data);
    $pr_typ->updatePartType($data);
    }
});


Flight::route('POST /adminger/parts/update',function()use($pr) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $data = Flight::request()->data;
    //print_r($data);
    $pr->updateParts($data);
    }
});

Flight::route('POST /adminger/mgr/update',function()use($u) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $data = Flight::request()->data;
    //print_r($data);
    $u->updateMGR($data);

    }
});


Flight::route('/adminger/user',function()use($m) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else
    $id = Flight::request()->query->id;
    print_r(json_encode($m->getMemberById($id)));
});



Flight::route('/adminger/area/list',function()use($area) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $country_id = Flight::request()->query->c_id;
    $country_name = Flight::request()->query->c_name;

    foreach($area->getArea($country_id) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['area_id'].'" type="radio"></th>
            <td>'.$val['area_name'].'</td>
            <td>'.$country_name.'</td>
            <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/></td></tr>';
    
    }

    }
});



Flight::route('/adminger/store/id',function()use($s) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else
    $id = Flight::request()->query->id;
    print_r(json_encode($s->getStoreById($id)));
});

Flight::route('/adminger/part_type/data',function()use($pr_typ) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    print_r(json_encode($pr_typ->getPartTypeByID($id)));
    }
});


Flight::route('/adminger/area/id',function()use($area) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->a_id;
    print_r(json_encode($area->getArea2($id)));
    }
});

Flight::route('/adminger/get_brand',function()use($br) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    print_r(json_encode($br->getBrandByID($id)));
    }
});

Flight::route('/adminger/get_service',function()use($serv) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    print_r(json_encode($serv->getServiceById($id)));
    }
});



Flight::route('/adminger/get_man',function()use($manuf) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    print_r(json_encode($manuf->getManByID($id)));
    }
});

Flight::route('/adminger/get_model',function()use($mo) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else
    $id = Flight::request()->query->id;
    print_r(json_encode($mo->getAllModulesById($id)));
});

Flight::route('/adminger/get_service_type',function()use($serv_type) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else{
    $id = Flight::request()->query->id;
    print_r(json_encode($serv_type->getServicesType($id)));
    }
});


Flight::route('/adminger/partsByID',function()use($pr) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else
    $id = Flight::request()->query->id;
    print_r(json_encode($pr->getPartstById($id)));
});

Flight::route('/adminger/edit_mgr',function()use($u) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
    }else
    $id = Flight::request()->query->id;
    print_r(json_encode($u->getMGRById($id)));
});


Flight::route('/adminger/signin', function(){

    session_destroy();
    Flight::render('login.php');

});

Flight::route('GET /adminger/add_area', function()use($area){
    
    $id = Flight::request()->query->c_id;     
    echo'<option value="0">أختر منطقة</option>';
    foreach ($area->getArea($id) as $val) {
        echo'<option value="'.$val['area_id'].'">'.$val['area_name'].'</option>';
    }

});


Flight::route('GET /adminger/add_model', function()use($mo){
    
    $id = Flight::request()->query->b_id;     
    echo'<option value="0">أختر موديل</option>';
    foreach ($mo->getModel($id) as $val) {
        echo'<option value="'.$val['mo_id'].'">'.$val['mo_title'].'</option>';
    }

});



Flight::route('/adminger/accounts', function()use($m){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else
     Flight::render('accounts.php',array(
         'types'=>$m->getTypes(),
         'countery'=>$m->getCountery(),
         'accounts'=>$m->accounts()
          )
        );
});

Flight::route('/adminger/management', function()use($u){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else
     Flight::render('mgr.php',array(
         'types'=>$u->getTypes(),
         'countery'=>$u->getCountery(),
         'accounts'=>$u->accounts()
          )
        );
});


Flight::route('GET /adminger/store/search', function()use($s){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $word = Flight::request()->query->word;

     //print_r($m->searchByName($word));

       foreach($s->searchByName($word) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['store_id'].'" type="radio"></th>
            <td>'.$val['store_title'].'</td>
            <td>'.$val['store_cdate'].'</td>
            <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/></td>
            <td>'.(($val['store_status']==0)?'<img src="../views/img/release.png" width="24" height="24" class="release_btn"  style="cursor:pointer;"/>':'<img src="../views/img/stop.png" width="24" height="24" class="del_btn"  style="cursor:pointer;"/>').'</td></tr>';
    
    }

    }
});

Flight::route('GET /adminger/specialization/search', function()use($specialization){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $word = Flight::request()->query->word;

     //print_r($m->searchByName($word));

       foreach($specialization->searchByName($word) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['spec_id'].'" type="radio"></th>
            <td>'.$val['spec_title'].'</td>
            <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/></td></tr>';
    
    }

    }
});

Flight::route('GET /adminger/store/filter', function()use($s){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $word = Flight::request()->query->word;

     //print_r($m->searchByName($word));

       foreach($s->searchStoreByName($word) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['store_id'].'" type="radio"></th>
            <td>'.$val['store_title'].'</td>
            <td>'.$val['store_cdate'].'</td>';
    
    }

    }
});


Flight::route('GET /adminger/service/list', function()use($serv){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $type = Flight::request()->query->stype;

       foreach($serv->getServicesByType($type) as $val){
        echo'<tr><td scope="row"><input class="accID" name="accID" value="'.$val['ser_id'].'" type="radio"></td>
            <td>'.$val['owner'].'</td>
            <td>'.$val['stype'].'</td>
            <td>'.((strlen($val['ser_details'])>20)?mb_substr($val['ser_details'],0,20).'...':$val['ser_details']).'</td>
            <td>'.$val['ser_date'].'</td>
            <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/></td>
            <td>'.(($val['ser_periods']==3)?'<img src="../views/img/release.png" width="24" height="24" class="release_btn"  style="cursor:pointer;"/>':'<img src="../views/img/stop.png" width="24" height="24" class="del_btn"  style="cursor:pointer;"/>').'</td></tr>';

    }

    }
});


Flight::route('GET /adminger/brand/search', function()use($br){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $word = Flight::request()->query->word;

       foreach($br->searchByName($word) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['b_id'].'" type="radio"></th>
        <td>'.$val['b_title'].'</td>
        <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/></td></tr>';
    }

    }
});


Flight::route('GET /adminger/model/search', function()use($mo){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $word = Flight::request()->query->word;

       foreach($mo->searchByName($word) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['mo_id'].'" type="radio"></th>
        <td>'.$val['mo_title'].'</td>
        <td>'.$val['btitle'].'</td>
        <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/></td></tr>';

      }

    }
});



Flight::route('GET /adminger/service_type/search', function()use($serv_type){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $word = Flight::request()->query->word;

       foreach($serv_type->searchByName($word) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['sert_id'].'" type="radio"></th>
        <td>'.$val['sert_type'].'</td>
        <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/></td></tr>';

      }

    }
});

Flight::route('GET /adminger/accounts/search', function()use($m){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $word = Flight::request()->query->word;

     //print_r($m->searchByName($word));

       foreach($m->searchByName($word) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['m_id'].'" type="radio"></th>
        <td>'.$val['m_account'].'</td>
        <td>'.$val['m_cdate'].'</td>
        <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" /></td>
        <td><img src="../views/img/stop.png" width="24" height="24" class="del_btn" /></td></tr>';
    }

    }
});


Flight::route('GET /adminger/man/search', function()use($manuf){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $word = Flight::request()->query->word;

     //print_r($m->searchByName($word));

       foreach($manuf->searchByName($word) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['mfr_id'].'" type="radio"></th>
        <td>'.$val['mfr_name'].'</td>
        <td>'.$val['mfr_details'].'</td>
        <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/></td></tr>';
    }

    }
});


Flight::route('GET /adminger/parts_type/search', function()use($pr_typ){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $word = Flight::request()->query->word;

     //print_r($m->searchByName($word));

       foreach($pr_typ->searchByName($word) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['prt_id'].'" type="radio"></th>
        <td>'.$val['prt_title'].'</td>
        <td>'.$val['prt_desc'].'</td>
        <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/></td>
        <td><img src="../views/img/del.png" width="24" height="24" class="release_btn"  style="cursor:pointer;"/></td></tr>';
    }

    }
});


Flight::route('GET /adminger/accounts/filter', function()use($m){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $word = Flight::request()->query->word;

     //print_r($m->searchByName($word));

       foreach($m->searchByNameV2($word) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['m_id'].'" type="radio"></th>
        <td>'.$val['m_account'].'</td>
        <td>'.$val['m_cdate'].'</td>';
    }

    }
});


Flight::route('GET /adminger/mgr/search', function()use($u){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $word = Flight::request()->query->word;

     //print_r($m->searchByName($word));

       foreach($u->searchByName($word) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['u_id'].'" type="radio"></th>
        <td>'.$val['u_account'].'</td>
        <td>'.$val['u_cdate'].'</td>
        <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" /></td>
        <td><img src="../views/img/stop.png" width="24" height="24" class="del_btn" /></td></tr>';
    }

    }
});

Flight::route('GET /adminger/mgr/filter', function()use($u){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $word = Flight::request()->query->word;

     //print_r($m->searchByName($word));

       foreach($u->searchByMGRName($word) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['u_id'].'" type="radio"></th>
        <td>'.$val['u_account'].'</td>
        <td>'.$val['u_cdate'].'</td>';
    }

    }
});



Flight::route('GET /adminger/cars/search', function()use($p){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $word = Flight::request()->query->word;

     //print_r($m->searchByName($word));

       foreach($p->searchByName($word) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['p_id'].'" type="radio"></th>
        <td>'.$val['p_name'].'</td>
        <td>'.$val['brand'].'</td>
        <td>'.$val['p_cdate'].'</td>
        <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/></td>
        <td><img src="../views/img/del.png" width="24" height="24" class="del_btn"  style="cursor:pointer;"/></td></tr>';
    }

    }
});


Flight::route('GET /adminger/parts/search', function()use($pr){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     $word = Flight::request()->query->word;

     //print_r($m->searchByName($word));

       foreach($pr->searchByName($word) as $val){
        echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['pr_id'].'" type="radio"></th>
            <td>'.$val['pr_name'].'</td>
            <td>'.$val['brand'].'</td>
            <td>'.$val['pr_cdate'].'</td>
            <td><img src="../views/img/edit.png" width="24" height="24" class="edit_btn" style="cursor:pointer;"/></td>
            <td><img src="../views/img/del.png" width="24" height="24" class="del_btn"  style="cursor:pointer;"/></td></tr>';     
    }

    }
});



Flight::route('/adminger/technicians', function() use ($portfo,$specialization){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else
    Flight::render('profile_acc.php',array('types'=>$portfo->get_technical(), 'specfic' => $specialization->getspecialization()));

});

Flight::route('/adminger/haraj', function()use($haraj){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
    Flight::render('haraj.php',[
        'haraj'=>$haraj->getHaraj(),
        'post_type'=>$haraj->getPostType(),
        'status'=>$haraj->getStatus(),
        'post_cat'=>$haraj->category()
    ]);
    }
});

Flight::route('/adminger/haraj_type', function()use($haraj){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
        Flight::render('haraj_type.php');
     } 
});

Flight::route('/adminger/haraj/filter', function()use($haraj){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
        foreach($haraj->getHarajByFilter(Flight::request()->query->post_type,Flight::request()->query->post_cat,Flight::request()->query->status) as $val){
            
            echo'<tr><th scope="row"><input class="accID" name="accID" value="'.$val['post_id'].'" type="radio"></th>
            <td>'.$val['post_title'].'</td>
            <td>'.$val['post_price'].'</td>
            <td>'.$val['currency'].'</td>
            <td>'.$val['status'].'</td>
            <td>'.$val['post_view_count'].'</td>
            <td><a href=""><b>'.$val['nick_name'].'</b></a></td>
            <td>'.$val['post_cdate'].'</td></tr>';
        }
        //$haraj->getHarajByFilter(Flight::request()->query->post_type,Flight::request()->query->post_cat,Flight::request()->query->status);
    }
});


Flight::route('/adminger/cars', function() use ($mo,$manuf,$m,$s,$fl_typ,$p){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{

      $brand = $mo->getBarnd();   
      $manuf = $manuf->getAllMan();
      $ownr_store = $m->getusr_store_owner();
      $curr = $m->get_curreny();
      $stor = $s->Get_store();
      $ful_type = $fl_typ->getfuel_type();
      $cars = $p->limit(15);

     Flight::render('cars.php',
      array(
      'barnd'=>$brand,
      'manuf'=>$manuf,
      'ownr_store' => $ownr_store,
     'curr' => $curr,
      'stor'=> $stor,
      'ful_type' => $ful_type,
      'cars'=> $cars 
    ));  

    }
});


Flight::route('/adminger/models', function() use ($mo){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{

    $brand = Flight::request()->query->brand_id;

      $models='<option value="0">أختر الموديل</option>';

      foreach ($mo->getModel($brand) as $val) {
          $models.='<option value="'.$val['mo_id'].'">'.$val['mo_title'].'</option>';
      }  
     echo $models;
    }
});


Flight::route('/adminger/services', function() use ($serv,$m,$s){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else
     Flight::render('services.php', array('serv_typ'=>$serv->getservice(),'owner_store' => $s->Get_store(),  
     'countery'=>$m->getCountery(),'service'=>$serv->getsallervice()));
});


Flight::route('/adminger/services_type', function()use($serv_type){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else
     Flight::render('services_type.php',[
         'service_type'=>$serv_type->getservices_type()
     ]);
});

Flight::route('/adminger/technicians_Specfic', function()use($specialization){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else
     Flight::render('technicians_specfic.php',[
         'spec'=>$specialization->getspecialization()
     ]);
});


Flight::route('/adminger/parts', function() use($mo,$manuf,$m,$pr_typ,$pr,$s) {
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else
    Flight::render('parts.php', array('barnd'=>$mo->getBarnd(), 'manuf'=>$manuf->getAllMan(),'curr' => $m->get_curreny(),
          'ownr_store' => $s->Get_store(), 'pr_typ' => $pr_typ->getparts_typ()
          , 'parts' => $pr->limit(20)));
});


Flight::route('/adminger/chang_pwd', function()use($m){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
        Flight::render('chang_pwd.php',array(
            'accounts'=>$m->accounts()
             )
           );
           //Flight::render('chang_pwd.php');
        }
     
});


Flight::route('POST /adminger/new_pwd', function()use($m){
    if(empty($_SESSION['uid'])){
        Flight::redirect('/adminger/signin');
        session_destroy();
     }else{
    $data = Flight::request()->data;
    if($m->update_pwd($data['m_id'],$data["m_pwd"],$data["o_u_pwd"])){
     echo '1';
    }else{
      echo '0';
    }
     }
});


Flight::route('/adminger/mgr_chang_pwd', function()use($u){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
        Flight::render('mgr_chang_pwd.php',array(
            'accounts'=>$u->accounts()
             )
           );
    }
    
});

Flight::route('POST /adminger/new_mgr_pwd', function()use($u){
    if(empty($_SESSION['uid'])){
        Flight::redirect('/adminger/signin');
        session_destroy();
     }else{
    $data = Flight::request()->data;
    if($u->update_pwd($data['u_id'],$data["u_pwd"],$data["o_u_pwd"])){
     echo '1';
    }else{
      echo '0';
    }
     }
});


Flight::route('/adminger/barnd', function()use($br){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else{
     Flight::render('add_barnd.php',array(
        'brands'=>$br->getBrands(),
        'brand_type'=>$br->getBrandType()
         ));
        }
});

Flight::route('/adminger/model', function() use ($mo){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else
     Flight::render('add_model.php',array(
        'brand'=>$mo->getBarnd(),
        'allModuels'=>$mo->getAllModules()
    ));
});


Flight::route('/adminger/manifctor', function() use ($manuf){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else
     Flight::render('add_manufacturer.php',array(
     'allMan'=>$manuf->getAllMan()
    ));
});

Flight::route('/adminger/parts_type', function()use($pr_typ){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else
     Flight::render('add_parts_type.php',array(
        'pr_type'=>$pr_typ->getparts_typ()));
});

Flight::route('/adminger/type', function(){
    if(empty($_SESSION['uid'])){
       Flight::redirect('/adminger/signin');
       session_destroy();
    }else
     Flight::render('add_product_type.php');
});


Flight::route('POST /adminger/auth', function()use($u){
     $data = Flight::request()->data;

   if($u->access([$data['u_account'],$data['u_pwd']])){
       // Flight::redirect('/adminger');
       echo '1';
    }else{
       //Flight::redirect('/adminger/signin');
       echo '0';
    }
       
});


/*Flight::route('/members/login', function()use($m){
    $data = Flight::request()->data;

    $m->login([$data['m_mobile'],$data['m_pwd']]);
});*/

Flight::route('/users/login', function()use($u){
    $data = Flight::request()->data;

    $u->access([$data['u_account'],$data['u_pwd']]);
});


Flight::route('POST /area/new', function()use($area){
    
    $data = Flight::request()->data;
    $area->insert([
        '',
        $data['area_name'],
        $data['area_latLang'],
        $data['country'],
    ]);

});


Flight::route('POST /add_type_haraj/new', function()use($p_typ){
    
    $data = Flight::request()->data;
    $p_typ->insert([
        '',
        $data['pt_type'],
        $data['pt_desc']
    ]);

});

Flight::route('POST /service_type/new', function()use($serv_type){
    
    $data = Flight::request()->data;
    $serv_type->insert([
        '',
        $data['sert_type']
    ]);

});


Flight::route('POST /specialization/new', function()use($specialization){
    
    $data = Flight::request()->data;
    $specialization->insert([
        '',
        $data['spec_title']
    ]);

});

Flight::route('POST /account/new', function()use($m){
    
    $data = Flight::request()->data;    
    $file =  Flight::request();
    $target_dir = "./files/";
    $target_file = $target_dir.rand(500,5000) .'_'. basename($file->files['m_img']['name']);
    $extension = strtolower(pathinfo($file->files['m_img']['name'], PATHINFO_EXTENSION));
    
    if($extension=='jpg' || $extension=='jpeg')
    {
        move_uploaded_file($file->files['m_img']["tmp_name"], $target_file);     
        $m->insert([
        '',
        $data['m_fname'],
        $data['m_lname'],
        $data['m_gender'],
        $data['m_status'],
        $data['m_account'],
        password_hash($data['m_pwd'],PASSWORD_BCRYPT),
        $data['m_mobile'],
        $data['m_email'],
        $data['m_country'],
        $data['m_area'],
        $data['m_address'],
        $data['m_utype'],
        date('Y-m-d'),
        $data['m_comment'],
        'http://bbsoft2world.com/cars/dashboard'.explode('.',$target_file)[1].'.'.explode('.',$target_file)[2]
       // $_SESSION['uid']
    ]);
}
else{
    echo 'Wrong ';
}

});

Flight::route('POST /mgr/new', function()use($u){
    
    $data = Flight::request()->data;    
    $file =  Flight::request();
    $target_dir = "./files/";
    $target_file = $target_dir.rand(500,5000) .'_'. basename($file->files['u_img']['name']);
    $extension = strtolower(pathinfo($file->files['u_img']['name'], PATHINFO_EXTENSION));
    
    if($extension=='jpg' || $extension=='jpeg')
    {
        move_uploaded_file($file->files['u_img']["tmp_name"], $target_file);     
        $u->insert([
        '',
        $data['u_fname'],
        $data['u_lname'],
        $data['u_gender'],
        $data['u_status'],
        $data['u_account'],
        password_hash($data['u_pwd'],PASSWORD_BCRYPT),
        $data['u_mobile'],
        $data['u_email'],
        $data['u_country'],
        $data['u_area'],
        $data['u_address'],
        $data['u_utype'],
        date('Y-m-d'),
        $data['u_comment'],
        'http://bbsoft2world.com/cars/dashboard'.explode('.',$target_file)[1].'.'.explode('.',$target_file)[2]
       // $_SESSION['uid']
    ]);
}
else{
    echo 'Wrong ';
}

});


Flight::route('POST /services/new', function()use($serv){
    
    $data = Flight::request()->data;    
    $file =  Flight::request();
    $target_dir = "./files/";
    $target_file = $target_dir.rand(500,5000) .'_'. basename($file->files['ser_thumb']['name']);
    $extension = strtolower(pathinfo($file->files['ser_thumb']['name'], PATHINFO_EXTENSION));
    
    if($extension=='jpg' || $extension=='jpeg')
    {
        move_uploaded_file($file->files['ser_thumb']["tmp_name"], $target_file); 
        $serv->insert([
        '',
        $data['ser_type'],
        $data['ser_periods'],
        $data['ser_address'],
        $data['ser_latlang'],
        $data['ser_owner'],
        $data['ser_details'],
        $data['ser_area'],
        date('Y-m-d'),
        $data['ser_country'],
        'http://bbsoft2world.com/cars/dashboard'.explode('.',$target_file)[1].'.'.explode('.',$target_file)[2],
        $data['ser_title'],
        1
    ]);
    }
    else{
        echo 'Wrong ';
    }

});

Flight::route('POST /product/new', function()use($p,$config){
    
    $data = Flight::request()->data;    
    
    $img_list='';
    //if(!is_string($data['p_img'])){
   
      $countfiles = count($_FILES['p_img']['name']);
   
      if($countfiles>0){
       for($i=0;$i<$countfiles;$i++){
         $new_img_name= 'img_'.rand(1,999999);
         $file_name=explode('.',$_FILES['p_img']['name'][$i]);
         $ext=end($file_name);
         $img_list.= $config['env']['PROJECT_PATH'].$config['env']['UPLOAD_DIR'].rtrim($new_img_name.'.'.$ext,',').',';
         move_uploaded_file($_FILES['p_img']['tmp_name'][$i],$config['env']['UPLOAD_DIR'].$new_img_name.'.'.$ext);
     
        }
       }else{
           $img_list='';
       }
    //}
//echo $img_list;
//print_r($_FILES['p_img']['name']);
    
  
       $p->insert([
        '',
        $data['p_name'],
        $data['is_sold'],//false,
        $data['p_model'],
        $data['vin_no'],
        $data['is_new'],
        $data['p_mfr'],
        $data['p_detailes'],
        $data['p_comment'], 
        date('Y-m-d'),
        $data['p_brand'],
        $data['p_currency'],
        $data['p_price'],
        //'http://bbsoft2world.com/cars/dashboard'.explode('.',$target_file)[1].'.'.explode('.',$target_file)[2],
        $img_list,
        $data['is_available'],
        $data['is_haraj'],
        11,
        "",
        "",
        1,
        $data["p_fuel"],
        "2020",
        $data['is_orginal'],
        $data["p_store"]            
       // $_SESSION['uid']
    ]);      

});


Flight::route('POST /parts/new', function()use($pr,$config){
    
    $data = Flight::request()->data;    
   

    $img_list='';
    //if(!is_string($data['p_img'])){
   
      $countfiles = count($_FILES['pr_img']['name']);
   
      if($countfiles>0){
       for($i=0;$i<$countfiles;$i++){
         $new_img_name= 'img_'.rand(1,999999);
         $file_name=explode('.',$_FILES['pr_img']['name'][$i]);
         $ext=end($file_name);
         $img_list.= $config['env']['PROJECT_PATH'].$config['env']['UPLOAD_DIR'].rtrim($new_img_name.'.'.$ext,',').',';
         move_uploaded_file($_FILES['pr_img']['tmp_name'][$i],$config['env']['UPLOAD_DIR'].$new_img_name.'.'.$ext);
     
        }
       }else{
           $img_list='';
       }

        $pr->insert([
            '',
            $data['pr_name'],
            $data['is_sold'],//false,
            $data['pr_model'],
            $data['vin_no'],
            $data['is_new'],
            $data['pr_mfr'],
            $data['pr_detailes'],
            $data['pr_comment'], 
            date('Y-m-d'),
            $data['pr_brand'],
            $data['pr_currency'],
            $data['pr_price'],
            $img_list,
            $data['is_available'],
            $data['is_haraj'],
            11,
            "",
            "2020",
            $data["pr_type"],
           $data['is_orginal'],
           $data["pr_store"]
        ]);
 
});


Flight::route('POST /members/new', function()use($m){
    
    $data = Flight::request()->data;
      
    $m->insert([
        '',
        $data['m_fname'],
        $data['m_lname'],
        $data['m_gender'],
        $data['m_status'],
        $data['m_account'],
        password_hash($data['m_pwd'],PASSWORD_BCRYPT),
        $data['m_mobile'],
        $data['m_email'],
        $data['m_country'],
        $data['m_area'],
        $data['m_address'],
        $data['m_utype'],
        $data['m_cdate'],
        $data['m_comment'],
        'img/unknow.jpg',
        $_SESSION['uid']
    ]);

});


Flight::route('POST /users/new', function()use($u){
    
    $data = Flight::request()->data;

    $u->insert([
        '',
        $data['u_fname'],
        $data['u_lname'],
        $data['u_gender'],
        $data['u_status'],
        $data['u_account'],
        password_hash($data['u_pwd'],PASSWORD_BCRYPT),
        $data['u_mobile'],
        $data['u_email'],
        $data['u_country'],
        $data['u_area'],
        $data['u_address'],
        $data['u_utype'],
        $data['u_cdate'],
        $data['u_comment'],
        'img/unknow.jpg'
    ]);

});

Flight::route('POST /brand/new', function()use($br){
    
    $data = Flight::request()->data;    
    $file =  Flight::request();
    $target_dir = "./files/";
    $target_file = $target_dir.rand(500,5000) .'_'. basename($file->files['b_image']['name']);
    $extension = strtolower(pathinfo($file->files['b_image']['name'], PATHINFO_EXTENSION));
    
    if($extension=='jpg' || $extension=='jpeg')
    {
        move_uploaded_file($file->files['b_image']["tmp_name"], $target_file);
        $br->insert([
        '',
        $data['b_title'],
        $data['b_def'],
        'http://bbsoft2world.com/cars/dashboard'.explode('.',$target_file)[1].'.'.explode('.',$target_file)[2],
        1,
        $data['b_type']
       // $_SESSION['uid']
    ]);
}else{
    echo 'Wrong ';
}
});


Flight::route('POST /adminger/service_update', function()use($serv){
    
    $data = Flight::request()->data;
   //print_r($data);
    $serv->updateService([
        $data['ser_id'],
        $data['ser_type'],
        $data['ser_periods'],
        $data['ser_address'],
        $data['ser_latlang'],
        $data['ser_owner'],
        $data['ser_details'],
        $data['ser_area'],
        $data['ser_country'],
        $data['ser_title']
       ]);
});

Flight::route('POST /adminger/brand_update', function()use($br){
    
    $data = Flight::request()->data;
   //print_r($data);
    $br->updateBrand([
        $data['b_id'],
       $data['b_title'],
        $data['b_def'],
        $data['b_type']
       ]);
});

Flight::route('POST /adminger/service_type_update', function()use($serv_type){
    
    $data = Flight::request()->data;
   $serv_type->updateServiceType([
        $data['sert_id'],
       $data['sert_type']
       ]);
});


Flight::route('POST /adminger/area/update', function()use($area){
    
    $data = Flight::request()->data;
   //print_r($data);
    $area->updateArea([
        $data['area_id'],
       $data['area_name'],
        $data['area_latLang'],
        $data['country']
       ]);
});

Flight::route('POST /adminger/man_update', function()use($manuf){
    
    $data = Flight::request()->data;
   //print_r($data);
    $manuf->updateMan([
        $data['mfr_id'],
       $data['mfr_name'],
        $data['mfr_details'],
        $data['mfr_comment']
       ]);
});


Flight::route('POST /adminger/model_update', function()use($mo){
    
    $data = Flight::request()->data;
   //print_r($data);
    $mo->updateModel([
        $data['mo_id'],
       $data['mo_title'],
        $data['b_title']
       ]);
});


Flight::route('POST /adminger/store/update', function()use($s){
    
    $data = Flight::request()->data;
   //print_r($data);
    $s->updateStore([
        $data['store_id'],
        $data['store_title'],
        $data['store_address'],
        $data['store_latlang'],
        $data['store_area'],
        $data['store_detial']
       ]);
});

Flight::route('POST /store/new', function()use($s){
    
    $data = Flight::request()->data;
    $s->insert([
        '',
        $data['store_title'],
        $data['store_address'],
        $data['store_latlang'],
        $data['store_owner'],
        date('Y-m-d'),
        $data['store_area'],
        1,
        $data['store_detial']
    ]);
});

Flight::route('POST /models/new', function()use($mo){
    
    $data = Flight::request()->data;
    $mo->insert([
        '',
        $data['mo_title'],
        $data['mo_brand']
    ]);
});

Flight::route('POST /serv_type/new', function()use($serv_type){
    
    $data = Flight::request()->data;
    $serv_type->insert([
        '',
        $data['sert_type']
    ]);
});

Flight::route('POST /manufctor/new', function()use($manuf){
    
    $data = Flight::request()->data;
    $manuf->insert([
        '',
        $data['mfr_name'],
        $data['mfr_details'],
        $data['mfr_comment']
    ]);
});

Flight::route('POST /Quantity/Insert', function()use($mk,$ked){
    
    $data = Flight::request()->data;
    $mk->insert([
        date('Y-m-d'),
        $data['op_sn'],
        $_SESSION['uid'],
        $data['p_sn'],
        $data['qty_in'],
        $data['qty_out'],
        $data['price'],
        $data['p_type']
    ]);

    $ked->insert([
        '',
        date('Y-m-d'),
        $data['op_sn'],
        $_SESSION['uid'],
        $data['dep'],
       ($data['qty_in'] * $data['price']),
      //  $data['cre'],
        $data['cur_sn'],
        0,
        0
    ]);
});

Flight::route('POST /part_type/new', function()use($pr_typ){
    
    $data = Flight::request()->data;

    $file =  Flight::request();
    $target_dir = "./files/";
    $target_file = $target_dir.rand(500,5000) .'_'. basename($file->files['prt_img']['name']);
    $extension = strtolower(pathinfo($file->files['prt_img']['name'], PATHINFO_EXTENSION));
    
    if($extension=='jpg' || $extension=='jpeg')
    {
        move_uploaded_file($file->files['prt_img']["tmp_name"], $target_file);

    $pr_typ->insert([
        '',
        $data['prt_title'],
        $data['prt_desc'],
        'http://bbsoft2world.com/cars/dashboard'.explode('.',$target_file)[1].'.'.explode('.',$target_file)[2],
        1
    ]);
    }else{
        echo 'إمتداد غير مدعوم';
    }
});

Flight::route('POST /product_type/new', function()use($fl_typ){
    
    $data = Flight::request()->data;
    $fl_typ->insert([
        '',
        $data['f_type']
       // $data['pt_desc']
       // $data['pt_comment']
    ]);
});

Flight::route('POST /product_type202/new', function()use($fl_typ){
    
    $data = Flight::request()->data;
    $fl_typ->insert([
        '',
        $data['pt_type'],
        $data['pt_desc']
       // $data['pt_comment']
    ]);
});

Flight::route('POST /portfo/new', function()use($portfo){
    
    $data = Flight::request()->data;
    $portfo->modify([
        $data['u_id'],
        $data['u_skills'],
        $data['u_cert'],
        $data['u_defination'],
        $data['u_accomplishment'],
        $data['u_spec']
    ],$data['u_id']);
});

Flight::route('/about', function(){
    echo 'About';
});

Flight::start();
