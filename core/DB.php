<?php

interface IconnectionManager
{
   public function init();
}


class MySQL implements IconnectionManager{
     
    public function __construct(){}
  
    public function init(){
        global $config;
        return new PDO('mysql:host='.$config['DB']['mysql']['host'].';dbname='.$config['DB']['mysql']['dbname'].';charset='.$config['DB']['mysql']['charset'], $config['DB']['mysql']['user'], $config['DB']['mysql']['password'], array(PDO::ATTR_EMULATE_PREPARES => false,PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
      }

}

class SQLite{}

class Postgre{}

class Database
{

 protected static $dbObject = null;
 protected static $db = null;
 
 private function __construct(){}
 private function __clone(){}


  public static function engage(IconnectionManager $db){

    if (self::$dbObject == null)
    {
      self::$dbObject = new Database();
      self::$dbObject = $db->init();
      //return self::$dbObject;       
      
    }else{
      self::$dbObject = null;
      echo 'Can`t create another instance...<br/>';
    }

    //return new static;
  }


  public function columns(){
    return array_keys(self::$dbObject->query('select * from '.strtolower(get_called_class()))->fetch(PDO::FETCH_ASSOC));
  }

  public static function all(){
    return (!empty(self::$dbObject))?self::$dbObject->query('select * from '.strtolower(get_called_class()))->fetchall(PDO::FETCH_ASSOC):null; 
  }

  public function getId($id){
   return self::$dbObject->query('select * from '.strtolower(get_called_class()).' where '.$this->columns()[0].'='.$id)->fetch(PDO::FETCH_ASSOC); 
  }

  public function login($data=[]){
    $res = self::$dbObject->query('select m_id, m_fname, m_lname, m_gender, m_status, m_account, m_pwd, m_mobile, m_email, country.c_name, area.area_name, m_address, m_utype, m_cdate, m_comment, m_img FROM members INNER join country ON members.m_country = country.c_id INNER JOIN area ON members.m_area = area.area_id WHERE members.m_mobile='.$data[0])->fetch(PDO::FETCH_ASSOC);
    //echo 'select m_id, m_fname, m_lname, m_gender, m_status, m_account, m_pwd, m_mobile, m_email, country.c_name, area.area_name, m_address, m_utype, m_cdate, m_comment, m_img FROM members INNER join country ON members.m_country = country.c_id INNER JOIN area ON members.m_area = area.area_id WHERE members.m_mobile='.$data[0];
    //echo $res[6].'---'.$data[0].'---'.$data[1];
    //echo $res[6];
    if(password_verify($data[1],$res['m_pwd']))
       print_r(json_encode($res));
    else
       print_r(['message'=>'Wrong mobile No / password','status'=>0]);
  }


  public function access($data=[]){
    $res = self::$dbObject->query('select u_id, u_fname, u_lname, u_gender, u_status, u_account, u_pwd, u_mobile, u_email, country.c_name, area.area_name, u_address, u_utype, u_cdate, u_comment, u_img FROM users INNER join country ON users.u_country = country.c_id INNER JOIN area ON users.u_area = area.area_id WHERE users.u_status=2 and users.u_account="'.$data[0].'"')->fetch(PDO::FETCH_ASSOC);
    //echo 'select m_id, m_fname, m_lname, m_gender, m_status, m_account, m_pwd, m_mobile, m_email, country.c_name, area.area_name, m_address, m_utype, m_cdate, m_comment, m_img FROM members INNER join country ON members.m_country = country.c_id INNER JOIN area ON members.m_area = area.area_id WHERE members.m_mobile='.$data[0];
    //echo $res[6].'---'.$data[0].'---'.$data[1];
    //echo $res[6];
    if(password_verify($data[1],$res['u_pwd'])){
       $_SESSION['uid']=$res['u_id'];
       return true;
    }else{
       return false;
    }
 }


 public static function insert($query = []){
  //get used class name as table name in query.
  //$table = end(explode('\\',get_called_class()));
  $sql='insert into '.strtolower(get_called_class()).'()values(';

  foreach($query as $val){
       $sql.='?,';
  }

  $sql=rtrim($sql,',');
  $sql.=')';

  //echo $sql;
 $res = self::$dbObject->prepare($sql);
  
 if($res->execute($query))
     echo '<center><div style="margin-top:120px;">Created successfully,<a href="../adminger"><b>Back to Dashboard</b></a></div></center>';
  else
     var_dump($res->errorCode());

}

public static function update($query = [],$chosenfield = []){
  
  $sql = 'update '.strtolower(get_called_class()).' set ';

  foreach($query as $key => $val){
       $sql.= $key."='".$val."',"  ;
  }
  $sql=rtrim($sql,',');
  $sql .= ' where ' ;

  foreach($chosenfield as $key => $val){
    $sql.= $key.'='.$val  ;
  }
//echo $sql;
 // $sql.=')';
 //echo $sql;
 $res = self::$dbObject->prepare($sql);
  
 if($res->execute())
     echo 'Update successfully  ^_^';
  else
     var_dump($res->errorCode());

}
}
?>
